import { DashboardOutlined,
  CarryOutOutlined,
  DesktopOutlined,
  SafetyCertificateOutlined,
  SettingOutlined,
  TableOutlined } from '@ant-design/icons';

// 菜单图标
export const IconMap = {
  'DashboardOutlined': <DashboardOutlined />,
  'CarryOutOutlined': <CarryOutOutlined />,
  'DesktopOutlined': <DesktopOutlined />,
  'SafetyCertificateOutlined': <SafetyCertificateOutlined />,
  'SettingOutlined': <SettingOutlined />,
  'TableOutlined': <TableOutlined />
};