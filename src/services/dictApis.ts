import { DictItem } from "@/pages/System/Dict/data";
import { PageQueryParamType, pageRequest, sendPostRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export interface DictQueryType extends PageQueryParamType {
  paramName?: string;
  paramKey?: string;
}

export async function fetchAllDict(params: DictQueryType) {
  const endPointURI = '/dev/sysdict/list';
  return pageRequest(endPointURI, params);
}

export async function addDict(params: DictItem) {
  const endPointURI = '/dev/sysdict/add';
  return sendPostRequest(endPointURI, params);
}

export async function updateDict(params: DictItem) {
  const endPointURI = '/dev/sysdict/update';
  return sendPostRequest(endPointURI, params);
}

export async function deleteDicts(params: DictItem) {
  const endPointURI = '/dev/sysdict/delete';
  return sendPostRequest(endPointURI, params);
}