import { RoleItem } from "@/pages/Base/Role/data";
import { PageQueryParamType, pageRequest, sendPostRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export interface RoleQueryType extends PageQueryParamType {
  paramName?: string;
  paramKey?: string;
}

export async function fetchAllRole(params: RoleQueryType) {
  const endPointURI = '/dev/role/list';
  return pageRequest(endPointURI, params);
}

export async function addRole(params: RoleItem) {
  const endPointURI = '/dev/role/add';
  return sendPostRequest(endPointURI, params);
}

export async function updateRole(params: RoleItem) {
  const endPointURI = '/dev/role/update';
  return sendPostRequest(endPointURI, params);
}

export async function deleteRoles(params: RoleItem) {
  const endPointURI = '/dev/role/delete';
  return sendPostRequest(endPointURI, params);
}

export async function grantAuths(params: {}) {
  const endPointURI = '/dev/role/allot/menu';
  return sendPostRequest(endPointURI, params);
}