import { sendGetJsonRequest } from "@/utils/requestEx";
import SmartStorageService from '@/utils/Storage';

const stoarge: SmartStorageService = SmartStorageService.getInstance('dict');
/**
 * 查询 dictName 对应的字典项
 * @param {*} data 参数
 * @returns dictName 对应的字典项
 */
export async function querySysDict(data: any) {
  const { dictName } = data;
  // 判断会话缓存中是否有缓存
  const dictList = stoarge.getItem(dictName);
  if (dictList) {
    // 会话缓存中存在，则取缓存，不再去服务端通讯
    console.info(`缓存中获取数据 ${dictName}`);
    return Promise.resolve(dictList);
  }
  const endPointURI = `/dev/sysdict/params/${dictName}`;
  const res = sendGetJsonRequest(endPointURI, data);
  res.then(data => {
    stoarge.setItem(dictName, data);
  });
  return res;
}