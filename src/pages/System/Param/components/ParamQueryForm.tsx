import React from 'react';
import { Form, Input } from 'antd';
import { basicFormItemLangLayout } from '@/utils/commons';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { QueryFormProps } from '@/models/FormModal';
import { ParamItem } from '../data';

export type ParamQueryProps = QueryFormProps<ParamItem> & {
}

const FormItem = Form.Item;

class ParamQueryForm extends React.PureComponent<ParamQueryProps> {

  render() {

    const { colon, loading, onSubmit, onReset } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={onSubmit}
        onReset={onReset}
      >
        <FormItem label='参数名称' name='paramName' {...formItemLayout} colon={colon}>
          <Input placeholder='支持模糊查询'/>
        </FormItem>
        <FormItem label='参数键名' name='paramKey' {...formItemLayout} colon={colon}>
          <Input placeholder='支持模糊查询'/>
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default ParamQueryForm;