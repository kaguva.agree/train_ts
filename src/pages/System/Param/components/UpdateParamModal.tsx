import React from 'react';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { formLayout, modalFormItemLayout, maxModalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { ParamItem } from '../data';
// import styles from './list.less';

type UpdateParamProps<T> = FormModalProps<T> & {
  /** 标识，view-查看，edit-更新 */
  flag: string;
}

const FormItem = Form.Item;

/**
 * 更新参数弹窗表单
 */
export default class UpdateParamModal extends React.PureComponent<UpdateParamProps<ParamItem>> {
  // useForm 是 React Hooks 的实现，只能用于函数组件
  // formRef = Form.useForm;
  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('UpdateParamModal.componentDidMount');
  }

  onOk = () => {
    const { formData, flag } = this.props;
    if (flag === 'view') {
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK(formData);
      }
    } else {
      const { paramId } = formData;
      console.log(this.formRef);
      this.formRef.current!.validateFields().then(fieldsValue => {
        console.info(fieldsValue);
        const values = {
          ...fieldsValue,
          paramId
        };
        console.log('Received values of form: ', values);
        const { onHandlerOK } = this.props;
        if (onHandlerOK) {
          onHandlerOK({
            ...values
          });
        }
      }).catch((err) => console.info('表单校验不通过'));
    }
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onHandlerCancel } = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("UpdateParamModal.render");
    const { formData, colon, modalWidth, modalVisible, loading, flag } = this.props;
    console.info(`flag: ${flag}`);
    const formItemLayout = modalFormItemLayout;
    const modalTitle = (flag === 'edit' ? '修改系统参数' : '查看系统参数信息');
    // const visible = (flag === 'edit' ? true : false);
    // const disabled = (flag === 'edit' ? false : true);

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label='参数名称' name='paramName' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '参数名称最多允许输入20个字符' },
                  { required: true, message: '参数名称必输' }
                ]}
                initialValue={formData.paramName}
              >
                <Input disabled/>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='参数键名' name='paramKey' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '参数键名最多允许输入20个字符' },
                  { required: true, message: '参数键名必输' },
                  { pattern: new RegExp(/^[A-Za-z][A-Za-z0-9\\._]{1,19}$/, "g"), message: '参数键名只能以字母开头，由A-Za-z0-9._组成，最大长度为20' }
                ]}
                initialValue={formData.paramKey}
              >
                <Input disabled/>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='参数键值' name='paramValue' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '参数键值最多允许输入20个字符' },
                  { required: true, message: '参数键值必输' },
                ]}
                initialValue={formData.paramValue}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <FormItem label='参数键值描述' name='paramValueDesc' {...maxModalFormItemLayout} colon={colon}
                rules={[
                  { max: 30, message: '参数键值描述最多允许输入30个字符' },
                  { required: true, message: '参数键值描述必输' }
                ]}
                initialValue={formData.paramValueDesc}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}