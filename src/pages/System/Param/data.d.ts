export interface ParamItem {
  paramId: string;
  paramName: string;
  paramKey: string;
  paramValue: string;
  paramValueDesc?: string;
  paramStates?: string;
}