import React from 'react';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, modalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { DictItem } from '../data';

type UpdateDictProps<T> = FormModalProps<T> & {
  /** 标识，view-查看，edit-更新 */
  flag: string;
  editFlagData: DataItem[];
  statusData: DataItem[];
}

const FormItem = Form.Item;

class UpdateDictModal extends React.PureComponent<UpdateDictProps<DictItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount(){
    console.info('UpdateDictModal.componentDidMount');
  }

  onOk = () => {
    const { formData, flag } = this.props;
    if (flag === 'view') {
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK(formData);
      }
    } else {
      const { dictId } = formData;
      console.log(this.formRef);
      this.formRef.current!.validateFields().then(fieldsValue => {
        console.info(fieldsValue);
        const values = {
          ...fieldsValue,
          dictId
        };
        console.log('Received values of form: ', values);
        const { onHandlerOK } = this.props;
        if (onHandlerOK) {
          onHandlerOK({
            ...values
          });
        }
      }).catch((err) => console.info('表单校验不通过'));
    }
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const {onHandlerCancel} = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("UpdateDictModal.render");
    const { formData, colon, modalWidth, modalVisible, editFlagData, statusData, loading, flag } = this.props;
    console.info(`flag: ${flag}`);
    const formItemLayout = modalFormItemLayout;
    const modalTitle = (flag === 'edit' ? '修改系统字典' : '查看系统字典信息');
    // const visible = (flag === 'edit' ? true : false);
    // const disabled = (flag === 'edit' ? false : true);

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        width={modalWidth}
        visible={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
        <Row>
            <Col span={12}>
              <FormItem label='字典名' name='dictName' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '字典名最多允许输入20个字符' },
                  { required: true, message: '字典名必输' }
                ]}
                initialValue={formData.dictName}
              >
                <Input disabled/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='字典描述' name='dictDesc' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '字典描述最多允许输入20个字符' },
                  { required: true, message: '字典描述必输' }
                ]}
                initialValue={formData.dictDesc}
              >
                <Input disabled/>
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='字典项' name='dictItemName' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '字典项最多允许输入20个字符' },
                  { required: true, message: '字典项必输' }
                ]}
                initialValue={formData.dictItemName}
              >
                <Input disabled/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='字典项描述' name='dictItemDesc' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '字典项描述最多允许输入20个字符' },
                  { required: true, message: '字典项描述必输' }
                ]}
                initialValue={formData.dictItemDesc}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="是否可编辑" name='editFlag' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '是否可编辑必输' }
                ]}
                initialValue='0'
              >
                <ASelect dataSource={editFlagData}/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="状态" name='status' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '状态必输' }
                ]}
                initialValue='0'
              >
                <ASelect dataSource={statusData}/>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}

export default UpdateDictModal;
