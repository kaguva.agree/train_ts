import React from 'react';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, modalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { DictItem } from '../data';

type AddDictProps<T> = FormModalProps<T> & {
  editFlagData: DataItem[];
  statusData: DataItem[];
}

const FormItem = Form.Item;

class AddDictModal extends React.PureComponent<AddDictProps<DictItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('AddDictModal.componentDidMount');
  }

  onOk = () => {
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const values = {
        ...fieldsValue
      };
      console.log('Received values of form: ', values);
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK({
          ...values
        });
      }
    }).catch();
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onHandlerCancel } = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("AddDictModal.render");
    // const colon = false;
    // const formLayout = 'horizontal'
    const { formData, colon, modalTitle, modalWidth, modalVisible, editFlagData, statusData, loading } = this.props;
    // const form = Form.useForm;
    // const { resetFields } = form;
    const formItemLayout = modalFormItemLayout;
    // 是否有表格勾选数据传进来，保证传进的值不为undefined
    console.info(formData);

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        maskClosable={false}
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label='字典名' name='dictName' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '字典名最多允许输入20个字符' },
                  { required: true, message: '字典名必输' }
                ]}
                initialValue={formData.dictName}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='字典描述' name='dictDesc' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '字典描述最多允许输入20个字符' },
                  { required: true, message: '字典描述必输' }
                ]}
                initialValue={formData.dictDesc}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='字典项' name='dictItemName' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '字典项最多允许输入20个字符' },
                  { required: true, message: '字典项必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='字典项描述' name='dictItemDesc' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '字典项描述最多允许输入20个字符' },
                  { required: true, message: '字典项描述必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="是否可编辑" name='editFlag' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '是否可编辑必输' }
                ]}
                initialValue='0'
              >
                <ASelect dataSource={editFlagData}/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="状态" name='status' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '状态必输' }
                ]}
                initialValue='0'
              >
                <ASelect dataSource={statusData}/>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}

export default AddDictModal;