import React from 'react';
import { Form, Input } from 'antd';
import { basicFormItemLangLayout } from '@/utils/commons';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { QueryFormProps } from '@/models/FormModal';
import { DictItem } from '../data';

export type DictQueryProps = QueryFormProps<DictItem> & {
}

const FormItem = Form.Item;

class DictQueryForm extends React.PureComponent<DictQueryProps> {

  render() {

    const { colon, loading, onSubmit, onReset } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={onSubmit}
        onReset={onReset}
      >
        <FormItem label='字典名' name='dictName' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label='字典描述' name='dictDesc' {...formItemLayout} colon={colon}>
          <Input placeholder='支持模糊查询'/>
        </FormItem>
        <FormItem label='字典项描述' name='dictItemDesc' {...formItemLayout} colon={colon}>
          <Input placeholder='支持模糊查询'/>
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default DictQueryForm;