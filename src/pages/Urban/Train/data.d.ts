import { Moment } from "moment";

export interface TrainItem {
  id: string;
  num: string;
  capacity: number;
  line: string;
  manufactureDate: string;
  lastMaintenanceDate: string;
  status: string;
  malfunction: string;
}

export interface TrainAddParams {
  num: string;
  capacity: string;
  line: string;
  manufactureDate: string;
}

export type TrainQueryFormType = {
  num?: string;
  line?: string;
  mftDate?: Moment[];
  lmtDate?: Moment[];
  status?: string;
  // mftStartDate: string;
  // mftEndDate: string;
  // lmStartDate: string;
  // lmEndDate: string;
}

export type TrainQueryParams = Omit<Partial<TrainQueryFormType>, 'mftDate' | 'lmtDate'> & {
  mftStartDate?: string;
  mftEndDate?: string;
  lmtStartDate?: string;
  lmtEndDate?: string;
}

export interface FormValueType extends Partial<TrainItem> {

}