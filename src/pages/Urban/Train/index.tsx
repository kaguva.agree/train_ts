import React, { useState, useEffect, Key } from 'react';
import { Drawer, Tag, Space } from 'antd';
import { connect, Dispatch } from 'umi';
import { TRAIN_LIST,
  TRAIN_ADD,
  TRAIN_UPDATE,
  TRAIN_DELETE,
  UPDATE_STATE } from '@/actions/train';
import ProCard from '@ant-design/pro-card';
import ProDescriptions, { ProDescriptionsItemProps } from '@ant-design/pro-descriptions';
import {PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import AButton from '@/components/SelfComp/AButton';
import { ConnectState } from '@/models/connect';
import { DataItem } from '@/models/common';
import { getItemValue } from '@/utils/commons';
import TrainQueryForm from './components/QueryForm';
import AddTrainModal from './components/AddModal';
import UpdateTrainModal from './components/UpdateModal';
import { TrainItem, TrainQueryParams } from './data';

interface TrainListProps {
  dispatch: Dispatch;
  trainList: TrainItem[],
  total: number,
  loading: boolean;
  addLoading: boolean;
  updateLoading: boolean;
  deleteLoading: boolean;
  statusData: DataItem[];
}

const EMPTY_TRAIN: TrainItem = {
  id: '',
  num: '',
  capacity: 0,
  line: '',
  manufactureDate: '',
  lastMaintenanceDate: '',
  status: '',
  malfunction: '',
}

const EMPTY_TRAIN_QUERY: TrainQueryParams = {
  num: '',
  line: '',
  status: '',
}

/**
 * 列车列表管理
 * @param props 属性
 * @returns 列车列表
 */
const TrainList: React.FC<TrainListProps> = (props) => {
  const code = 'train-list';
  const { trainList, loading, total, addLoading, updateLoading, deleteLoading, statusData } = props;
  const [addModalVisible, handleAddModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [row, setRow] = useState<TrainItem>();
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [selectedRows, setSelectedRows] = useState<TrainItem[]>([]);
  const [queryParam, setQueryParam] = useState<TrainQueryParams>(EMPTY_TRAIN_QUERY);
  const [pageSize, setPageSize] = useState<number>(10);
  const [trainFormData, setTrainFormData] = useState<TrainItem>(EMPTY_TRAIN);
  const rowKey = (record: TrainItem) => {
    return record.id;
  }
  const pkField = 'id';
  const columns = [
    {
      title: '列车编号',
      dataIndex: 'num',
      render: (text: string, record: TrainItem, index: number) => {
        return <a onClick={() => setRow(record)}>{text}</a>;
      },
    },
    {
      title: '座位容量',
      dataIndex: 'capacity',
    },
    {
      title: '线路信息',
      dataIndex: 'line',
    },
    {
      title: '制造日期',
      dataIndex: 'manufactureDate',
      render: (text: string, record: TrainItem, index: number) => formatDate(text)
    },
    {
      title: '最后维护日期',
      dataIndex: 'lastMaintenanceDate',
      render: (text: string, record: TrainItem, index: number) => formatDate(text)
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: (text: string, record: TrainItem, index: number) => trainStatusFunc(record)
    },
    {
      title: '故障原因',
      dataIndex: 'malfunction',
      ellipsis: true
    },
  ];

  useEffect(() => {
    console.info('TrainList.useEffect');
    // fetchAllTrain(1, 10, queryParam);
    return () => {
      const { dispatch } = props;
      dispatch(UPDATE_STATE({
        rows: [],
        total: 0
      }));
    }
  }, []);

  const trainStatusFunc = (record: TrainItem) => {
    const { statusData } = props;
    const { status } = record;
    const value = getItemValue(statusData, status);
    if (status === '1') {
      return <Tag color='error'>{value}</Tag>;
    }
    return <Tag color='processing'>{value}</Tag>;
  }

  const formatDate = (dateStr: string) => {
    if (dateStr && dateStr.length === 8) {
      const year = dateStr.substring(0, 4);
      const month = dateStr.substring(4, 6);
      const day = dateStr.substring(6, 8);
      return `${year}-${month}-${day}`;
    }
    return dateStr;
  }

  /**
   * 查询满足条件的数据字典
   */
  const handleQuery = (record: TrainQueryParams) => {
    console.info('TrainList.handleQuery');
    console.info(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    fetchAllTrain(1, pageSize, queryParam);
  }

  const handleReset = () => {
    // 是否清空表格
  }

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  const fetchAllTrain = (pageNum: number, pageSize: number, queryParam: TrainQueryParams) => {
    const { dispatch } = props;
    dispatch(TRAIN_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 删除列车，支持多笔删除
   */
  const deleteTrains = async () => {
    const { dispatch } = props;
    const trainIds = [...selectedRowKeys]
    const res = await dispatch(TRAIN_DELETE(trainIds));
    if (res) {
      dispatch(TRAIN_LIST({
        ...queryParam,
        pageSize,
        pageNum: 1,
      }));
    }
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  const handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<TrainItem>) => {
    console.log('handleBtnCallBack');
    console.log(callBackModel);
    // btn 按钮
    // keys 表格勾选数组
    const { btn } = callBackModel;
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      handleAddModalVisible(true);
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      setTrainFormData(rows[0]);
      handleUpdateModalVisible(true);
      return;
    }
    // 删除
    if (alias === 'delete') {
      const { keys } = callBackModel;
      // 调用删除服务，删除勾选数据
      const trainId= keys[0];
      deleteTrain(trainId);
      return;
    }
  }

  /**
   * 删除列车，暂时只支持单笔删除
   * @param trainId 列车id
   */
  const deleteTrain = async (trainId: Key) => {
    const { dispatch } = props;
    const trainIds = [trainId]
    const res = await dispatch(TRAIN_DELETE(trainIds));
    if (res) {
      dispatch(TRAIN_LIST({
        ...queryParam,
        pageSize,
        pageNum: 1,
      }));
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  const onSelectRow = (keys: React.Key[], rows: TrainItem[]) => {
    setSelectedRowKeys(keys);
    setSelectedRows(rows);
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  const onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
    setPageSize(pageSize);
    fetchAllTrain(page, pageSize, queryParam);
  };

  const handleAddModal = async (record: TrainItem) => {
    console.log(record);
    const { dispatch } = props;
    console.info(dispatch);
    const res = await dispatch(TRAIN_ADD(record));
    console.info(res);
    if (res) {
      handleAddModalVisible(false);
      fetchAllTrain(1, pageSize, queryParam);
    }
  }

  const handleAddModalCancel = () => {
    handleAddModalVisible(false);
  };

  /**
   * 更新面板的确定事件
   * @param {*} record 更新表单
   */
  const handleUpdateModal = async (record: TrainItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    console.info(trainFormData);
    const { dispatch } = props;
    const res = await dispatch(TRAIN_UPDATE(record));
    console.info(res);
    if (res) {
      handleUpdateModalVisible(false);
      setTrainFormData(EMPTY_TRAIN);
      fetchAllTrain(1, pageSize, queryParam);
    }
  }

  const handleUpdateModalCancel = () => {
    handleUpdateModalVisible(false);
    setTrainFormData(EMPTY_TRAIN);
  };

  return (
    <PageContainer>
      <Space direction='vertical' size='middle' style={{ display: 'flex' }}>
        <ProCard title="查询条件" headerBordered>
          <TrainQueryForm
            statusData={statusData}
            colon={false}
            loading={loading}
            onSubmit={(record) => {
              setQueryParam(record);
              handleQuery(record);
            }}
            onReset={handleReset}
          />
        </ProCard>
        <ProCard>
          <AGrid
            code={code}
            btnCallBack={handleBtnCallBack}
            columns={columns}
            rowKey={rowKey}
            pkField={pkField}
            dataSource={trainList}
            loading={loading}
            total={total}
            onSelectRow={onSelectRow}
            onPageNumAndSizeChange={onPageNumAndSizeChange}
          />
        </ProCard>
      </Space>
      {selectedRows?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              已选择{' '}
              <a style={{ fontWeight: 600 }}>{selectedRows.length}</a>{' '}
              项
              &nbsp;&nbsp;
            </div>
          }
        >
          <AButton
            type='primary'
            code='batchDel'
            pageCode={code}
            name='批量删除'
            loading={deleteLoading}
            onClick={deleteTrains}
          />
        </FooterToolbar>
      )}
      {
        !addModalVisible ? null :
        <AddTrainModal
          statusData={statusData}
          colon={false}
          modalTitle='新增列车'
          modalWidth={1000}
          modalVisible={addModalVisible}
          loading={addLoading}
          onHandlerOK={handleAddModal}
          onHandlerCancel={handleAddModalCancel}
        />
      }
      {
        !updateModalVisible ? null :
        <UpdateTrainModal
          statusData={statusData}
          colon={false}
          modalTitle=''
          modalWidth={1000}
          modalVisible={updateModalVisible}
          flag='edit'
          loading={updateLoading}
          formData={trainFormData}
          onHandlerOK={handleUpdateModal}
          onHandlerCancel={handleUpdateModalCancel}
        />
      }
      <Drawer
        width={600}
        open={!!row}
        onClose={() => {
          setRow(undefined);
        }}
        closable={false}
      >
        {row?.id && (
          <ProDescriptions<TrainItem>
            column={2}
            title={row?.num}
            request={async () => ({
              data: row || {},
            })}
            params={{
              id: row?.id,
            }}
            columns={columns as ProDescriptionsItemProps<TrainItem>[]}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default connect((state: ConnectState) => ({
  trainList: state.trains.rows,
  total: state.trains.total,
  statusData: state.trains.statusData,
  loading: state.loading.effects['trains/fetchAllTrain'],
  addLoading: state.loading.effects['trains/addTrain'],
  updateLoading: state.loading.effects['trains/updateTrain'],
  deleteLoading: state.loading.effects['dicts/deleteTrains'],
}))(TrainList);