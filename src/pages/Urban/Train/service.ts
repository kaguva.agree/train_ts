import { TrainItem, TrainAddParams } from './data.d';
import { PageQueryParamType, pageRequest, sendPostRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export type TrainQueryType = Partial<TrainItem> & PageQueryParamType;

export async function fetchAllTrain(params: TrainQueryType) {
  return pageRequest('/api/train/list', params);
}

export async function addTrain(params: TrainAddParams) {
  return sendPostRequest('/api/train/insert', params);
}

export async function updateTrain(params: TrainItem) {
  return sendPostRequest('/api/train/update', params);
}

export async function deleteTrains(params: { id: string[] }) {
  return sendPostRequest('/api/train/delete', params);
}