import React from 'react';
import { Modal, Form, Row, Col, Input, DatePicker } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { formLayout, modalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { TrainItem } from '../data';

type AddTrainProps<T> = FormModalProps<T> & {
  statusData: DataItem[];
}
type AddTrainPropsEx<T> = Omit<AddTrainProps<T>, 'formData'>;

const FormItem = Form.Item;
const dateFormatPattern = 'YYYY-MM-DD';
const dateFormatPattern2 = 'YYYYMMDD';

class AddTrainModal extends React.PureComponent<AddTrainPropsEx<TrainItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('AddTrainModal.componentDidMount');
  }

  onOk = () => {
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const { manufactureDate, ...rest } = fieldsValue;
      const values = {
        manufactureDate: manufactureDate.format(dateFormatPattern2),
        ...rest
      };
      console.log('Received values of form: ', values);
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK({
          ...values
        });
      }
    }).catch();
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onHandlerCancel } = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("AddTrainModal.render");
    // const colon = false;
    // const formLayout = 'horizontal'
    const { colon, modalTitle, modalWidth, modalVisible, loading } = this.props;
    // const form = Form.useForm;
    // const { resetFields } = form;
    const formItemLayout = modalFormItemLayout;
    // 是否有表格勾选数据传进来，保证传进的值不为undefined

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        maskClosable={false}
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label='列车编号' name='num' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '列车编号最长为 20 个字符' },
                  { required: true, message: '列车编号必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='座位容量' name='capacity' {...formItemLayout} colon={colon}
                rules={[
                  { max: 4, message: '座位容量最长为 4 个字符' },
                  { required: true, message: '座位容量必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='线路信息' name='line' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '线路信息最多允许输入 20 个字符' },
                ]}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='制造日期' name='manufactureDate' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '制造日期必输' }
                ]}
              >
                <DatePicker format={dateFormatPattern} style={{ width: '100%' }} />
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}

export default AddTrainModal;