import React from 'react';
import { Form, Input, DatePicker } from 'antd';
import ASelect from '@/components/SelfComp/ASelect';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { DataItem } from '@/models/common';
import { QueryFormProps } from '@/models/FormModal';
import { basicFormItemLangLayout } from '@/utils/commons';
import { TrainQueryFormType, TrainQueryParams } from '../data';

export type TrainQueryProps = QueryFormProps<TrainQueryParams> & {
  statusData: DataItem[];
}

const FormItem = Form.Item;
const { RangePicker } = DatePicker;
const dateFormat: string = 'YYYY-MM-DD';

class TrainQueryForm extends React.PureComponent<TrainQueryProps> {

  handleSubmit = (record: TrainQueryFormType) => {
    const { mftDate, lmtDate, ...res } = record
    let mftStartDate = '';
    let mftEndDate = '';
    let lmtStartDate = '';
    let lmtEndDate = '';
    if (mftDate) {
      mftStartDate = mftDate[0].format(dateFormat).replaceAll('-', '');
      mftEndDate = mftDate[1].format(dateFormat).replaceAll('-', '');
    }
    if (lmtDate) {
      lmtStartDate = lmtDate[0].format(dateFormat).replaceAll('-', '');
      lmtEndDate = lmtDate[1].format(dateFormat).replaceAll('-', '');
    }
    const newRecord: TrainQueryParams = {
      mftStartDate,
      mftEndDate,
      lmtStartDate,
      lmtEndDate,
      ...res
    }
    const { onSubmit } = this.props;
    onSubmit({
      ...newRecord
    });
  }

  render() {
    const { colon, loading, onReset, statusData } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={this.handleSubmit}
        onReset={onReset}
      >
        <FormItem label='列车编号' name='num' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label='线路信息' name='line' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label="制造时间" name='mftDate' {...formItemLayout} colon={colon}>
          <RangePicker format={dateFormat} style={{ width: '100%' }} />
        </FormItem>
        <FormItem label="最后维修时间" name='lmtDate' {...formItemLayout} colon={colon}>
          <RangePicker format={dateFormat} style={{ width: '100%' }} />
        </FormItem>
        <FormItem label="状态" name='status' {...formItemLayout} colon={colon}>
          <ASelect dataSource={statusData} />
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default TrainQueryForm;