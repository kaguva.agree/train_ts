import React from 'react';
import { Modal, Form, Row, Col, Input, DatePicker } from 'antd';
import moment from 'moment';
import { FormInstance } from 'antd/es/form/Form';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, modalFormItemLayout, maxModalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { TrainItem } from '../data';

type UpdateTrainProps<T> = FormModalProps<T> & {
  /** 标识，view-查看，edit-更新 */
  flag?: string;
  statusData: DataItem[];
}

const FormItem = Form.Item;
const dateFormatPattern = 'YYYY-MM-DD';
const dateFormatPattern2 = 'YYYYMMDD';
const { TextArea } = Input;

export default class UpdateTrainModal extends React.PureComponent<UpdateTrainProps<TrainItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('UpdateTrainModal.componentDidMount');
  }

  onOk = () => {
    const { formData, flag } = this.props;
    if (flag === 'view') {
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK(formData);
      }
    } else {
      const { id } = formData;
      this.formRef.current!.validateFields().then(fieldsValue => {
        console.info(fieldsValue);
        let { manufactureDate, lastMaintenanceDate, ...rest } = fieldsValue;
        if (lastMaintenanceDate) {
          lastMaintenanceDate = lastMaintenanceDate.format(dateFormatPattern2)
        }
        if (manufactureDate) {
          manufactureDate = manufactureDate.format(dateFormatPattern2)
        }
        const values = {
          id,
          manufactureDate,
          lastMaintenanceDate,
          ...rest
        };
        console.log('Received values of form: ', values);
        const { onHandlerOK } = this.props;
        if (onHandlerOK) {
          onHandlerOK({
            ...values
          });
        }
      }).catch((err) => console.info('表单校验不通过'));
    }
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const {onHandlerCancel} = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("UpdateTrainModal.render");
    const { formData, colon, modalWidth, modalVisible, loading, flag = 'edit', statusData } = this.props;
    console.info(`flag: ${flag}`);
    const formItemLayout = modalFormItemLayout;
    const modalTitle = (flag === 'edit' ? '修改列车信息' : '查看列车信息');
    let { manufactureDate, lastMaintenanceDate } = formData;
    if (!manufactureDate) {
      manufactureDate = '';
    }
    if (!lastMaintenanceDate) {
      lastMaintenanceDate = '';
    }

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label="列车编号" name='num' {...formItemLayout} colon={colon}
                initialValue={formData.num}
              >
                <Input disabled />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="座位容量" name='capacity' {...formItemLayout} colon={colon}
                rules={[
                  { max: 4, message: '座位容量最长为 4 个字符' },
                  { required: true, message: '座位容量必输' }
                ]}
                initialValue={formData.capacity}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="线路信息" name='line' {...formItemLayout} colon={colon}
                initialValue={formData.line}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="制造日期" name='manufactureDate' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '制造日期必输' }
                ]}
                initialValue={manufactureDate === '' ? null : moment(formData.manufactureDate, dateFormatPattern)}
              >
                <DatePicker format={dateFormatPattern} style={{ width: '100%' }} />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="最后维护日期" name='lastMaintenanceDate' {...formItemLayout} colon={colon}
                initialValue={lastMaintenanceDate === '' ? null : moment(lastMaintenanceDate, dateFormatPattern)}
              >
                <DatePicker format={dateFormatPattern} style={{ width: '100%' }} />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="状态" name='status' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '状态必输' },
                ]}
                initialValue={formData.status}
              >
                <ASelect dataSource={statusData} />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <FormItem label="故障原因" name='malfunction' {...maxModalFormItemLayout} colon={colon}
                rules={[
                  { max: 30, message: '故障原因最多允许输入30个字符' }
                ]}>
                <TextArea showCount maxLength={30} />
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}