import React from 'react';
import { Form, Input } from 'antd';
import ASelect from '@/components/SelfComp/ASelect';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { DataItem } from '@/models/common';
import { QueryFormProps } from '@/models/FormModal';
import { basicFormItemLangLayout } from '@/utils/commons';
import { LineItem } from '../data';

export type LineQueryProps = QueryFormProps<LineItem> & {
  statusData: DataItem[];
}

const FormItem = Form.Item;

class LineQueryForm extends React.PureComponent<LineQueryProps> {

  handleSubmit = (record: LineItem) => {
    const { onSubmit } = this.props;
    onSubmit({
      ...record
    });
  }

  render() {
    const { colon, loading, onReset, statusData } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={this.handleSubmit}
        onReset={onReset}
      >
        <FormItem label='线路编码' name='code' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label='线路名称' name='name' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label="状态" name='status' {...formItemLayout} colon={colon}>
          <ASelect dataSource={statusData} />
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default LineQueryForm;