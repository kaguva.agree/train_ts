import React from 'react';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { formLayout, modalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { LineItem } from '../data';
import ASelect from '@/components/SelfComp/ASelect';

type AddLineProps<T> = FormModalProps<T> & {
  directionData: DataItem[];
  stationData: DataItem[];
  statusData: DataItem[];
}
type AddLinePropsEx<T> = Omit<AddLineProps<T>, 'formData'>;

const FormItem = Form.Item;

class AddLineModal extends React.PureComponent<AddLinePropsEx<LineItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('AddLineModal.componentDidMount');
  }

  onOk = () => {
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const values = {
        ...fieldsValue
      };
      console.log('Received values of form: ', values);
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK({
          ...values
        });
      }
    }).catch();
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onHandlerCancel } = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("AddLineModal.render");
    // const colon = false;
    // const formLayout = 'horizontal'
    const { colon, modalTitle, modalWidth, modalVisible, loading, directionData, stationData, statusData } = this.props;
    // const form = Form.useForm;
    // const { resetFields } = form;
    const formItemLayout = modalFormItemLayout;
    // 是否有表格勾选数据传进来，保证传进的值不为undefined
    console.info(stationData);

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        maskClosable={false}
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label='线路编码' name='code' {...formItemLayout} colon={colon}
                rules={[
                  { max: 30, message: '线路编码最长为 30 个字符' },
                  { required: true, message: '线路编码必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='线路名称' name='name' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '线路名称最长为 20 个字符' },
                  { required: true, message: '线路名称必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='运行方向' name='direction' {...formItemLayout} colon={colon}
                tooltip='下行表示从线路的起点向终点方向的行驶'
                rules={[
                  { required: true, message: '运行方向必输' }
                ]}
              >
                <ASelect dataSource={directionData} />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='状态' name='status' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '状态必输' }
                ]}
              >
                <ASelect dataSource={statusData} />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='起始站点' name='startStation' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '起始站点必输' }
                ]}
              >
                <ASelect dataSource={stationData} />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='终点站点' name='endStation' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '终点站点必输' }
                ]}
              >
                <ASelect dataSource={stationData} />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='总里程' name='totalLength' {...formItemLayout} colon={colon}
                rules={[
                  { max: 5, message: '总里程最长为 5 个字符' },
                  { required: true, message: '总里程必输' }
                ]}
              >
                <Input addonAfter='（公里）' />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='运营间隔' name='interval' {...formItemLayout} colon={colon}
                rules={[
                  { max: 3, message: '运营间隔最长为 3 个字符' },
                  { required: true, message: '运营间隔必输' }
                ]}
              >
                <Input addonAfter='（分钟）' />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='最小票价' name='minTicket' {...formItemLayout} colon={colon}
                rules={[
                  { max: 5, message: '最小票价最长为 5 个字符' },
                  { required: true, message: '最小票价必输' }
                ]}
              >
                <Input addonAfter='（元）' />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='最大票价' name='maxTicket' {...formItemLayout} colon={colon}
                rules={[
                  { max: 5, message: '最大票价最长为 5 个字符' },
                  { required: true, message: '最大票价必输' }
                ]}
              >
                <Input addonAfter='（元）' />
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}

export default AddLineModal;