export interface LineItem {
  id: string;
  code: string;
  name: string;
  // 首先是9号线，由曹路站开往松江南站
  startStation: string;
  endStation: string;
  direction: string;
  totalLength: string;
  interval: number;
  minTicket: string;
  maxTicket: string;
  status: string;
}