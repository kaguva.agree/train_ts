import { LineItem } from './data.d';
import { PageQueryParamType, pageRequest, sendPostRequest, sendGetJsonRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export type LineQueryType = Partial<LineItem> & PageQueryParamType;

export async function fetchAllLine(params: LineQueryType) {
  return pageRequest('/api/line/list', params);
}

export async function addLine(params: LineItem) {
  return sendPostRequest('/api/line/insert', params);
}

export async function updateLine(params: LineItem) {
  return sendPostRequest('/api/line/update', params);
}

export async function deleteLines(params: { id: string[] }) {
  return sendPostRequest('/api/line/delete', params);
}

export async function fetchAllLineForSelect(params: { }) {
  // 判断会话缓存中是否有缓存
  // const stationData = userStoarge.getItem('station');
  // if (stationData) {
    // 会话缓存中存在，则取缓存，不再去服务端通讯
    // return Promise.resolve(stationData);
  // }
  const res = sendGetJsonRequest('/api/line/select', params);
  // res.then(data => {
    // userStoarge.setItem('station', data);
  // });
  return res;
}