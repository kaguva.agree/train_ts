import { StationItem } from './data.d';
import { PageQueryParamType, pageRequest, sendGetJsonRequest, sendPostRequest } from "@/utils/requestEx";
// import { userStoarge } from '@/utils/Storage';

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export type StationQueryType = Partial<StationItem> & PageQueryParamType;

export async function fetchAllStation(params: StationQueryType) {
  return pageRequest('/api/station/list', params);
}

export async function addStation(params: StationItem) {
  return sendPostRequest('/api/station/insert', params);
}

export async function updateStation(params: StationItem) {
  return sendPostRequest('/api/station/update', params);
}

export async function deleteStations(params: { id: string[] }) {
  return sendPostRequest('/api/station/delete', params);
}

export async function fetchAllStationForSelect(params: { }) {
  // 判断会话缓存中是否有缓存
  // const stationData = userStoarge.getItem('station');
  // if (stationData) {
    // 会话缓存中存在，则取缓存，不再去服务端通讯
    // return Promise.resolve(stationData);
  // }
  const res = sendGetJsonRequest('/api/station/select', params);
  // res.then(data => {
    // userStoarge.setItem('station', data);
  // });
  return res;
}