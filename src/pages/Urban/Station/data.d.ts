export interface StationItem {
  id: string;
  code: string;
  name: string;
  latitude: string;
  longitude: string;
  address: string
  status: string;
}