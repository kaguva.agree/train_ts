import React from 'react';
import { Form, Input } from 'antd';
import ASelect from '@/components/SelfComp/ASelect';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { DataItem } from '@/models/common';
import { QueryFormProps } from '@/models/FormModal';
import { basicFormItemLangLayout } from '@/utils/commons';
import { StationItem } from '../data';

export type StationQueryProps = QueryFormProps<StationItem> & {
  statusData: DataItem[];
}

const FormItem = Form.Item;

class StationQueryForm extends React.PureComponent<StationQueryProps> {

  handleSubmit = (record: StationItem) => {
    const { onSubmit } = this.props;
    onSubmit({
      ...record
    });
  }

  render() {
    const { colon, loading, onReset, statusData } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={this.handleSubmit}
        onReset={onReset}
      >
        <FormItem label='站点编码' name='code' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label='站点名称' name='name' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label="状态" name='status' {...formItemLayout} colon={colon}>
          <ASelect dataSource={statusData} />
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default StationQueryForm;