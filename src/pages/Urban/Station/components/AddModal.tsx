import React from 'react';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { formLayout, modalFormItemLayout, maxModalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { StationItem } from '../data';
import ASelect from '@/components/SelfComp/ASelect';

type AddStationProps<T> = FormModalProps<T> & {
  statusData: DataItem[];
}
type AddStationPropsEx<T> = Omit<AddStationProps<T>, 'formData'>;

const FormItem = Form.Item;

class AddStationModal extends React.PureComponent<AddStationPropsEx<StationItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('AddStationModal.componentDidMount');
  }

  onOk = () => {
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const values = {
        ...fieldsValue
      };
      console.log('Received values of form: ', values);
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK({
          ...values
        });
      }
    }).catch();
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onHandlerCancel } = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("AddStationModal.render");
    // const colon = false;
    // const formLayout = 'horizontal'
    const { colon, modalTitle, modalWidth, modalVisible, loading, statusData } = this.props;
    // const form = Form.useForm;
    // const { resetFields } = form;
    const formItemLayout = modalFormItemLayout;
    // 是否有表格勾选数据传进来，保证传进的值不为undefined

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        maskClosable={false}
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label='站点编码' name='code' {...formItemLayout} colon={colon}
                rules={[
                  { max: 30, message: '站点编码最长为 30 个字符' },
                  { required: true, message: '站点编码必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='站点名称' name='name' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '站点名称最长为 20 个字符' },
                  { required: true, message: '站点名称必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='经度' name='latitude' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '经度最长为 20 个字符' },
                  { required: true, message: '经度必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='纬度' name='longitude' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '纬度最长为 20 个字符' },
                  { required: true, message: '纬度必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <FormItem label='站点地址' name='address' {...maxModalFormItemLayout} colon={colon}
                rules={[
                  { max: 50, message: '站点地址最长为 50 个字符' },
                  { required: true, message: '站点地址必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='状态' name='status' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '站点名称必输' }
                ]}
              >
                <ASelect dataSource={statusData} />
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}

export default AddStationModal;