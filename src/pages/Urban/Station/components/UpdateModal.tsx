import React from 'react';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, modalFormItemLayout, maxModalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { StationItem } from '../data';

type UpdateStationProps<T> = FormModalProps<T> & {
  /** 标识，view-查看，edit-更新 */
  flag?: string;
  statusData: DataItem[];
}

const FormItem = Form.Item;

export default class UpdateStationModal extends React.PureComponent<UpdateStationProps<StationItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('UpdateStationModal.componentDidMount');
  }

  onOk = () => {
    const { formData, flag } = this.props;
    if (flag === 'view') {
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK(formData);
      }
    } else {
      const { id } = formData;
      this.formRef.current!.validateFields().then(fieldsValue => {
        console.info(fieldsValue);
        const values = {
          id,
          ...fieldsValue
        };
        console.log('Received values of form: ', values);
        const { onHandlerOK } = this.props;
        if (onHandlerOK) {
          onHandlerOK({
            ...values
          });
        }
      }).catch((err) => console.info('表单校验不通过'));
    }
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const {onHandlerCancel} = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("UpdateStationModal.render");
    const { formData, colon, modalWidth, modalVisible, loading, flag = 'edit', statusData } = this.props;
    console.info(`flag: ${flag}`);
    const formItemLayout = modalFormItemLayout;
    const modalTitle = (flag === 'edit' ? '修改站点信息' : '查看站点信息');

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}
          initialValues={{
            code: formData.code,
            name: formData.name,
            latitude: formData.latitude,
            longitude: formData.longitude,
            address: formData.address,
            status: formData.status
          }}
        >
          <Row>
            <Col span={12}>
              <FormItem label='站点编码' name='code' {...formItemLayout} colon={colon}
                rules={[
                  { max: 30, message: '站点编码最长为 30 个字符' },
                  { required: true, message: '站点编码必输' }
                ]}
              >
                <Input disabled />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='站点名称' name='name' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '站点名称最长为 20 个字符' },
                  { required: true, message: '站点名称必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='经度' name='latitude' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '经度最长为 20 个字符' },
                  { required: true, message: '经度必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='纬度' name='longitude' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '纬度最长为 20 个字符' },
                  { required: true, message: '纬度必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <FormItem label='站点地址' name='address' {...maxModalFormItemLayout} colon={colon}
                rules={[
                  { max: 50, message: '站点地址最长为 50 个字符' },
                  { required: true, message: '站点地址必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='状态' name='status' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '站点名称必输' }
                ]}
              >
                <ASelect dataSource={statusData} />
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}