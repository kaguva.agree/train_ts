import React, { useState, useEffect, Key } from 'react';
import { Drawer, Tag, Space } from 'antd';
import { connect, Dispatch } from 'umi';
import { STATION_LIST,
  STATION_ADD,
  STATION_UPDATE,
  STATION_DELETE,
  UPDATE_STATE } from '@/actions/station';
import ProCard from '@ant-design/pro-card';
import ProDescriptions, { ProDescriptionsItemProps } from '@ant-design/pro-descriptions';
import {PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import AButton from '@/components/SelfComp/AButton';
import { ConnectState } from '@/models/connect';
import { DataItem } from '@/models/common';
import { getItemValue } from '@/utils/commons';
import StationQueryForm from './components/QueryForm';
import AddStationModal from './components/AddModal';
import UpdateStationModal from './components/UpdateModal';
import { StationItem } from './data';

interface StationListProps {
  dispatch: Dispatch;
  stationList: StationItem[],
  total: number,
  loading: boolean;
  addLoading: boolean;
  updateLoading: boolean;
  deleteLoading: boolean;
  statusData: DataItem[];
}

const EMPTY_STATION: StationItem = {
  id: '',
  code: '',
  name: '',
  latitude: '',
  longitude: '',
  address: '',
  status: ''
}

/**
 * 站点列表管理
 * @param props 属性
 * @returns 站点列表
 */
const StationList: React.FC<StationListProps> = (props) => {
  const code = 'station-list';
  const { stationList, loading, total, addLoading, updateLoading, deleteLoading, statusData } = props;
  const [addModalVisible, handleAddModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [row, setRow] = useState<StationItem>();
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [selectedRows, setSelectedRows] = useState<StationItem[]>([]);
  const [queryParam, setQueryParam] = useState<StationItem>(EMPTY_STATION);
  const [pageSize, setPageSize] = useState<number>(10);
  const [stationFormData, setStationFormData] = useState<StationItem>(EMPTY_STATION);
  const rowKey = (record: StationItem) => {
    return record.id;
  }
  const pkField = 'id';
  const columns = [
    {
      title: '站点编码',
      dataIndex: 'code',
      render: (text: string, record: StationItem, index: number) => {
        return <a onClick={() => setRow(record)}>{text}</a>;
      },
    },
    {
      title: '站点名称',
      dataIndex: 'name',
    },
    {
      title: '纬度',
      dataIndex: 'latitude',
    },
    {
      title: '经度',
      dataIndex: 'longitude',
    },
    {
      title: '站点地址',
      dataIndex: 'address',
      ellipsis: true
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: (text: string, record: StationItem, index: number) => stationStatusFunc(record)
    },
  ];

  useEffect(() => {
    console.info('StationList.useEffect');
    // fetchAllStation(1, 10, queryParam);
    return () => {
      const { dispatch } = props;
      dispatch(UPDATE_STATE({
        rows: [],
        total: 0
      }));
    }
  }, []);

  const stationStatusFunc = (record: StationItem) => {
    const { statusData } = props;
    const { status } = record;
    const value = getItemValue(statusData, status);
    if (status === '1') {
      return <Tag color='error'>{value}</Tag>;
    }
    return <Tag color='processing'>{value}</Tag>;
  }

  /**
   * 查询满足条件的数据字典
   */
  const handleQuery = (record: StationItem) => {
    console.info('StationList.handleQuery');
    console.info(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    fetchAllStation(1, pageSize, queryParam);
  }

  const handleReset = () => {
    // 是否清空表格
  }

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  const fetchAllStation = (pageNum: number, pageSize: number, queryParam: StationItem) => {
    const { dispatch } = props;
    dispatch(STATION_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 删除站点，支持多笔删除
   */
  const deleteStations = async () => {
    const { dispatch } = props;
    const stationId = [...selectedRowKeys]
    const res = await dispatch(TRAIN_DELETE(stationId));
    if (res) {
      dispatch(STATION_LIST({
        ...queryParam,
        pageSize,
        pageNum: 1,
      }));
    }
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  const handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<StationItem>) => {
    console.log('handleBtnCallBack');
    console.log(callBackModel);
    // btn 按钮
    // keys 表格勾选数组
    const { btn } = callBackModel;
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      handleAddModalVisible(true);
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      setStationFormData(rows[0]);
      handleUpdateModalVisible(true);
      return;
    }
    // 删除
    if (alias === 'delete') {
      const { keys } = callBackModel;
      // 调用删除服务，删除勾选数据
      const stationId= keys[0];
      deleteTrain(stationId);
      return;
    }
  }

  /**
   * 删除站点，暂时只支持单笔删除
   * @param stationId 站点id
   */
  const deleteTrain = async (stationId: Key) => {
    const { dispatch } = props;
    const stationIds = [stationId]
    const res = await dispatch(STATION_DELETE(stationIds));
    if (res) {
      dispatch(STATION_LIST({
        ...queryParam,
        pageSize,
        pageNum: 1,
      }));
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  const onSelectRow = (keys: React.Key[], rows: StationItem[]) => {
    setSelectedRowKeys(keys);
    setSelectedRows(rows);
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  const onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
    setPageSize(pageSize);
    fetchAllStation(page, pageSize, queryParam);
  };

  const handleAddModal = async (record: StationItem) => {
    console.log(record);
    const { dispatch } = props;
    console.info(dispatch);
    const res = await dispatch(STATION_ADD(record));
    console.info(res);
    if (res) {
      handleAddModalVisible(false);
      fetchAllStation(1, pageSize, queryParam);
    }
  }

  const handleAddModalCancel = () => {
    handleAddModalVisible(false);
  };

  /**
   * 更新面板的确定事件
   * @param {*} record 更新表单
   */
  const handleUpdateModal = async (record: StationItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    console.info(stationFormData);
    const { dispatch } = props;
    const res = await dispatch(STATION_UPDATE(record));
    console.info(res);
    if (res) {
      handleUpdateModalVisible(false);
      setStationFormData(EMPTY_STATION);
      fetchAllStation(1, pageSize, queryParam);
    }
  }

  const handleUpdateModalCancel = () => {
    handleUpdateModalVisible(false);
    setStationFormData(EMPTY_STATION);
  };

  return (
    <PageContainer>
      <Space direction='vertical' size='middle' style={{ display: 'flex' }}>
        <ProCard title="查询条件" headerBordered>
          <StationQueryForm
            statusData={statusData}
            colon={false}
            loading={loading}
            onSubmit={(record) => {
              setQueryParam(record);
              handleQuery(record);
            }}
            onReset={handleReset}
          />
        </ProCard>
        <ProCard>
          <AGrid
            code={code}
            btnCallBack={handleBtnCallBack}
            columns={columns}
            rowKey={rowKey}
            pkField={pkField}
            dataSource={stationList}
            loading={loading}
            total={total}
            onSelectRow={onSelectRow}
            onPageNumAndSizeChange={onPageNumAndSizeChange}
          />
        </ProCard>
      </Space>
      {selectedRows?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              已选择{' '}
              <a style={{ fontWeight: 600 }}>{selectedRows.length}</a>{' '}
              项
              &nbsp;&nbsp;
            </div>
          }
        >
          <AButton
            type='primary'
            code='batchDel'
            pageCode={code}
            name='批量删除'
            loading={deleteLoading}
            onClick={deleteStations}
          />
        </FooterToolbar>
      )}
      {
        !addModalVisible ? null :
        <AddStationModal
          statusData={statusData}
          colon={false}
          modalTitle='新增站点'
          modalWidth={1000}
          modalVisible={addModalVisible}
          loading={addLoading}
          onHandlerOK={handleAddModal}
          onHandlerCancel={handleAddModalCancel}
        />
      }
      {
        !updateModalVisible ? null :
        <UpdateStationModal
          statusData={statusData}
          colon={false}
          modalTitle=''
          modalWidth={1000}
          modalVisible={updateModalVisible}
          flag='edit'
          loading={updateLoading}
          formData={stationFormData}
          onHandlerOK={handleUpdateModal}
          onHandlerCancel={handleUpdateModalCancel}
        />
      }
      <Drawer
        width={600}
        open={!!row}
        onClose={() => {
          setRow(undefined);
        }}
        closable={false}
      >
        {row?.id && (
          <ProDescriptions<StationItem>
            column={2}
            title={row?.code}
            request={async () => ({
              data: row || {},
            })}
            params={{
              id: row?.id,
            }}
            columns={columns as ProDescriptionsItemProps<StationItem>[]}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default connect((state: ConnectState) => ({
  stationList: state.stations.rows,
  total: state.stations.total,
  statusData: state.stations.statusData,
  loading: state.loading.effects['stations/fetchAllStation'],
  addLoading: state.loading.effects['stations/addStation'],
  updateLoading: state.loading.effects['stations/updateStation'],
  deleteLoading: state.loading.effects['dicts/deleteStations'],
}))(StationList);