import { LineOpeItem } from './data.d';
import { PageQueryParamType, pageRequest, sendPostRequest } from "@/utils/requestEx";

/**
 * 分页查询参条件
 * @param paramName 可输，参数名称
 * @param paramKey 可输，参数键名
 */
export type LineOpeQueryType = Partial<LineOpeItem> & PageQueryParamType;

export async function fetchAllLineOpe(params: LineOpeQueryType) {
  return pageRequest('/api/line/ope/list', params);
}

export async function addLineOpe(params: LineOpeItem) {
  return sendPostRequest('/api/line/ope/insert', params);
}

export async function updateLineOpe(params: LineOpeItem) {
  return sendPostRequest('/api/line/ope/update', params);
}

export async function deleteLineOpes(params: { id: string[] }) {
  return sendPostRequest('/api/line/ope/delete', params);
}