import React from 'react';
import { Modal, Form, Row, Col, Input, InputNumber, TimePicker } from 'antd';
import moment from 'moment';
import { FormInstance } from 'antd/es/form/Form';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, modalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { LineOpeItem } from '../data';

type UpdateLineProps<T> = FormModalProps<T> & {
  /** 标识，view-查看，edit-更新 */
  flag?: string;
  stationData: DataItem[];
  statusData: DataItem[];
}

const FormItem = Form.Item;
const dateFormatPattern2 = 'HH:mm:ss';

export default class UpdateLineModal extends React.PureComponent<UpdateLineProps<LineOpeItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('UpdateLineModal.componentDidMount');
  }

  onOk = () => {
    const { formData, flag } = this.props;
    if (flag === 'view') {
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK(formData);
      }
    } else {
      const { id } = formData;
      this.formRef.current!.validateFields().then(fieldsValue => {
        console.info(fieldsValue);
        const { firstBusTime, lastBusTime, ...rest } = fieldsValue;
        const values = {
          id,
          firstBusTime: firstBusTime.format(dateFormatPattern2),
          lastBusTime: lastBusTime.format(dateFormatPattern2),
          ...rest
        };
        console.log('Received values of form: ', values);
        const { onHandlerOK } = this.props;
        if (onHandlerOK) {
          onHandlerOK({
            ...values
          });
        }
      }).catch((err) => console.info('表单校验不通过'));
    }
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const {onHandlerCancel} = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("UpdateLineModal.render");
    const { formData, colon, modalWidth, modalVisible, loading, flag = 'edit', stationData, statusData } = this.props;
    console.info(`flag: ${flag}`);
    const formItemLayout = modalFormItemLayout;
    const modalTitle = (flag === 'edit' ? '修改线路运营信息' : '查看线路运营信息');

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}
          initialValues={{
            code: formData.code,
            name: formData.name,
            sort: formData.sort,
            station: formData.station,
            firstBusTime: formData.firstBusTime === '' ? null : moment(formData.firstBusTime, dateFormatPattern2),
            lastBusTime: formData.lastBusTime === '' ? null : moment(formData.lastBusTime, dateFormatPattern2),
            status: formData.status
          }}
        >
          <Row>
            <Col span={12}>
              <FormItem label='线路编码' name='code' {...formItemLayout} colon={colon}
                rules={[
                  { max: 30, message: '线路编码最长为 30 个字符' },
                  { required: true, message: '线路编码必输' }
                ]}
              >
                <Input disabled />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='线路名称' name='name' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '线路名称最长为 20 个字符' },
                  { required: true, message: '线路名称必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='站点序号' name='sort' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '站点序号必输' }
                ]}
              >
                <InputNumber min={1} max={100} precision={0} style={{ width: '100%' }} />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='站点名称' name='station' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '站点名称必输' }
                ]}
              >
                <ASelect dataSource={stationData} />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='首班车发车时间' name='firstBusTime' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '首班车发车时间必输' }
                ]}
              >
                <TimePicker style={{ width: '100%' }} />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label='末班车发车时间' name='lastBusTime' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '末班车发车时间必输' }
                ]}
              >
                <TimePicker style={{ width: '100%' }} />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label='状态' name='status' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '状态必输' }
                ]}
              >
                <ASelect dataSource={statusData} />
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}