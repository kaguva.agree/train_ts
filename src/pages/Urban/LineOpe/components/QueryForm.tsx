import React from 'react';
import { Form, Input } from 'antd';
import ASelect from '@/components/SelfComp/ASelect';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { DataItem } from '@/models/common';
import { QueryFormProps } from '@/models/FormModal';
import { basicFormItemLangLayout } from '@/utils/commons';
import { LineOpeItem } from '../data';

export type LineQueryProps = QueryFormProps<LineOpeItem> & {
  directionData: DataItem[];
  statusData: DataItem[];
  lineData: DataItem[];
}

const FormItem = Form.Item;

class LineQueryForm extends React.PureComponent<LineQueryProps> {

  handleSubmit = (record: LineOpeItem) => {
    const { onSubmit } = this.props;
    onSubmit({
      ...record
    });
  }

  render() {
    const { colon, loading, onSubmit, onReset, directionData, statusData, lineData } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={onSubmit}
        onReset={onReset}
      >
        <FormItem label='线路编码' name='code' {...formItemLayout} colon={colon}
          rules={[
            {required: true, message: '线路编码必输'}
          ]}
        >
          <ASelect dataSource={lineData} />
        </FormItem>
        <FormItem label="运行方向" name='direction' {...formItemLayout} colon={colon}
          rules={[
            {required: true, message: '运行方向必输'}
          ]}
        >
          <ASelect dataSource={directionData} value={'1'} />
        </FormItem>
        <FormItem label="状态" name='status' {...formItemLayout} colon={colon}>
          <ASelect dataSource={statusData} />
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default LineQueryForm;