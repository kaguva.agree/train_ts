import React, { useState, useEffect, Key } from 'react';
import { Drawer, Tag, Space } from 'antd';
import { connect, Dispatch } from 'umi';
import { LINEOPE_INIT,
  LINEOPE_LIST,
  LINEOPE_ADD,
  LINEOPE_UPDATE,
  LINEOPE_DELETE,
  UPDATE_STATE } from '@/actions/lineOpe';
import ProCard from '@ant-design/pro-card';
import ProDescriptions, { ProDescriptionsItemProps } from '@ant-design/pro-descriptions';
import {PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import AButton from '@/components/SelfComp/AButton';
import { ConnectState } from '@/models/connect';
import { DataItem } from '@/models/common';
import { getItemValue } from '@/utils/commons';
import LineOpeQueryForm from './components/QueryForm';
import AddLineOpeModal from './components/AddModal';
import UpdateLineOpeModal from './components/UpdateModal';
import { LineOpeItem } from './data';

interface LineOpeListProps {
  dispatch: Dispatch;
  lineOpeList: LineOpeItem[],
  total: number,
  loading: boolean;
  addLoading: boolean;
  updateLoading: boolean;
  deleteLoading: boolean;
  directionData: DataItem[];
  stationData: DataItem[];
  statusData: DataItem[];
  lineData: DataItem[];
}

const EMPTY_LINEOPE: LineOpeItem = {
  id: '',
  code: '',
  name: '',
  direction: '',
  sort: 0,
  station: '',
  firstBusTime: '',
  lastBusTime: '',
  status: ''
}

/**
 * 线路列表管理
 * @param props 属性
 * @returns 线路列表
 */
const LineOpeList: React.FC<LineOpeListProps> = (props) => {
  const code = 'line-ope-list';
  const { lineOpeList, loading, total, addLoading, updateLoading, deleteLoading,
    directionData, stationData, statusData, lineData } = props;
  const [addModalVisible, handleAddModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [row, setRow] = useState<LineOpeItem>();
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [selectedRows, setSelectedRows] = useState<LineOpeItem[]>([]);
  const [queryParam, setQueryParam] = useState<LineOpeItem>(EMPTY_LINEOPE);
  const [pageSize, setPageSize] = useState<number>(10);
  const [lineOpeFormData, setLineOpeFormData] = useState<LineOpeItem>(EMPTY_LINEOPE);
  const rowKey = (record: LineOpeItem) => {
    return record.id;
  }
  const pkField = 'id';
  const columns = [
    {
      title: '线路编码',
      dataIndex: 'code',
      render: (text: string, record: LineOpeItem, index: number) => {
        return <a onClick={() => setRow(record)}>{text}</a>;
      },
    },
    {
      title: '线路名称',
      dataIndex: 'name',
    },
    {
      title: '站点序号',
      dataIndex: 'sort',
    },
    {
      title: '站点名称',
      dataIndex: 'station',
      render: (text: string, record: LineOpeItem, index: number) => stationFunc(text, record)
    },
    {
      title: '首班车发车时间',
      dataIndex: 'firstBusTime',
    },
    {
      title: '末班车发车时间',
      dataIndex: 'lastBusTime',
    },
    {
      title: '状态',
      dataIndex: 'status',
      render: (text: string, record: LineOpeItem, index: number) => lineOpeStatusFunc(record)
    },
  ];

  useEffect(() => {
    console.info('LineOpeList.useEffect');
    const { dispatch } = props;
    dispatch(LINEOPE_INIT({}))
    return () => {
      const { dispatch } = props;
      dispatch(UPDATE_STATE({
        rows: [],
        total: 0
      }));
    }
  }, []);

  const lineOpeStatusFunc = (record: LineOpeItem) => {
    const { statusData } = props;
    const { status } = record;
    const value = getItemValue(statusData, status);
    if (status === '1') {
      return <Tag color='error'>{value}</Tag>;
    }
    return <Tag color='processing'>{value}</Tag>;
  }

  const stationFunc = (text: string, record: LineOpeItem) => {
    const { stationData } = props;
    const value = getItemValue(stationData, text, '2');
    return value;
  }

  /**
   * 查询满足条件的数据字典
   */
  const handleQuery = (record: LineOpeItem) => {
    console.info('LineOpeList.handleQuery');
    console.info(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    fetchAllLineOpe(1, pageSize, queryParam);
  }

  const handleReset = () => {
    // 是否清空表格
  }

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  const fetchAllLineOpe = (pageNum: number, pageSize: number, queryParam: LineOpeItem) => {
    console.info('fetchAllLineOpe', queryParam);
    const { dispatch } = props;
    dispatch(LINEOPE_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 删除线路运营，支持多笔删除
   */
  const deleteLineOpes = async () => {
    const { dispatch } = props;
    const lids = [...selectedRowKeys]
    const res = await dispatch(LINEOPE_DELETE(lids));
    if (res) {
      dispatch(LINEOPE_LIST({
        ...queryParam,
        pageSize,
        pageNum: 1,
      }));
    }
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  const handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<LineOpeItem>) => {
    console.log('handleBtnCallBack');
    console.log(callBackModel);
    // btn 按钮
    // keys 表格勾选数组
    const { btn } = callBackModel;
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      handleAddModalVisible(true);
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      setLineOpeFormData(rows[0]);
      handleUpdateModalVisible(true);
      return;
    }
    // 删除
    if (alias === 'delete') {
      const { keys } = callBackModel;
      // 调用删除服务，删除勾选数据
      const lids= keys[0];
      deleteLineOpe(lids);
      return;
    }
  }

  /**
   * 删除线路运营，暂时只支持单笔删除
   * @param lid 线路运营id
   */
  const deleteLineOpe = async (lid: Key) => {
    const { dispatch } = props;
    const lids = [lid]
    const res = await dispatch(LINEOPE_DELETE(lids));
    if (res) {
      dispatch(LINEOPE_LIST({
        ...queryParam,
        pageSize,
        pageNum: 1,
      }));
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  const onSelectRow = (keys: React.Key[], rows: LineOpeItem[]) => {
    setSelectedRowKeys(keys);
    setSelectedRows(rows);
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  const onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
    setPageSize(pageSize);
    fetchAllLineOpe(page, pageSize, queryParam);
  };

  const handleAddModal = async (record: LineOpeItem) => {
    console.log(record);
    const { dispatch } = props;
    console.info(dispatch);
    const res = await dispatch(LINEOPE_ADD(record));
    console.info(res);
    if (res) {
      handleAddModalVisible(false);
      fetchAllLineOpe(1, pageSize, queryParam);
    }
  }

  const handleAddModalCancel = () => {
    handleAddModalVisible(false);
  };

  /**
   * 更新面板的确定事件
   * @param {*} record 更新表单
   */
  const handleUpdateModal = async (record: LineOpeItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    console.info(lineOpeFormData);
    const { dispatch } = props;
    const res = await dispatch(LINEOPE_UPDATE(record));
    console.info(res);
    if (res) {
      handleUpdateModalVisible(false);
      setLineOpeFormData(EMPTY_LINEOPE);
      fetchAllLineOpe(1, pageSize, queryParam);
    }
  }

  const handleUpdateModalCancel = () => {
    handleUpdateModalVisible(false);
    setLineOpeFormData(EMPTY_LINEOPE);
  };

  return (
    <PageContainer>
      <Space direction='vertical' size='middle' style={{ display: 'flex' }}>
        <ProCard title="查询条件" headerBordered>
          <LineOpeQueryForm
            lineData={lineData}
            directionData={directionData}
            statusData={statusData}
            colon={false}
            loading={loading}
            onSubmit={(record) => {
              setQueryParam(record);
              handleQuery(record);
            }}
            onReset={handleReset}
          />
        </ProCard>
        <ProCard>
          <AGrid
            code={code}
            btnCallBack={handleBtnCallBack}
            columns={columns}
            rowKey={rowKey}
            pkField={pkField}
            dataSource={lineOpeList}
            loading={loading}
            total={total}
            onSelectRow={onSelectRow}
            onPageNumAndSizeChange={onPageNumAndSizeChange}
          />
        </ProCard>
      </Space>
      {selectedRows?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              已选择{' '}
              <a style={{ fontWeight: 600 }}>{selectedRows.length}</a>{' '}
              项
              &nbsp;&nbsp;
            </div>
          }
        >
          <AButton
            type='primary'
            code='batchDel'
            pageCode={code}
            name='批量删除'
            loading={deleteLoading}
            onClick={deleteLineOpes}
          />
        </FooterToolbar>
      )}
      {
        !addModalVisible ? null :
        <AddLineOpeModal
          stationData={stationData}
          statusData={statusData}
          colon={false}
          modalTitle='新增线路运营计划'
          modalWidth={1000}
          modalVisible={addModalVisible}
          loading={addLoading}
          onHandlerOK={handleAddModal}
          onHandlerCancel={handleAddModalCancel}
        />
      }
      {
        !updateModalVisible ? null :
        <UpdateLineOpeModal
          stationData={stationData}
          statusData={statusData}
          colon={false}
          modalTitle=''
          modalWidth={1000}
          modalVisible={updateModalVisible}
          flag='edit'
          loading={updateLoading}
          formData={lineOpeFormData}
          onHandlerOK={handleUpdateModal}
          onHandlerCancel={handleUpdateModalCancel}
        />
      }
      <Drawer
        width={600}
        open={!!row}
        onClose={() => {
          setRow(undefined);
        }}
        closable={false}
      >
        {row?.id && (
          <ProDescriptions<LineOpeItem>
            column={2}
            title={row?.code}
            request={async () => ({
              data: row || {},
            })}
            params={{
              id: row?.id,
            }}
            columns={columns as ProDescriptionsItemProps<LineOpeItem>[]}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default connect((state: ConnectState) => ({
  lineOpeList: state.lineopes.rows,
  total: state.lineopes.total,
  lineData: state.lineopes.lineData,
  directionData: state.lineopes.directionData,
  stationData: state.lineopes.stationData,
  statusData: state.lineopes.statusData,
  loading: state.loading.effects['lineopes/fetchAllLineOpe'],
  addLoading: state.loading.effects['lineopes/addLineOpe'],
  updateLoading: state.loading.effects['lineopes/updateLineOpe'],
  deleteLoading: state.loading.effects['lineopes/deleteLineOpes'],
}))(LineOpeList);