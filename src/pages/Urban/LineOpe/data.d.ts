export interface LineOpeItem {
  id: string;
  code: string;
  name: string;
  direction: string;
  sort: number;
  // 首先是9号线，由曹路站开往松江南站
  station: string;
  firstBusTime: string;
  lastBusTime: string;
  status: string;
}