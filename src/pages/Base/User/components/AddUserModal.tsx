import React from 'react';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import ASelect from '@/components/SelfComp/ASelect';
import { formLayout, modalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { UserItem } from '../data';


type AddUserProps<T> = FormModalProps<T> & {
  sexData: DataItem[];
  statusData: DataItem[];
}
type AddUserPropsEx<T> = Omit<AddUserProps<T>, 'formData'>;

const FormItem = Form.Item;

export default class AddUserModal extends React.PureComponent<AddUserPropsEx<UserItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('AddUserModal.componentDidMount');
  }

  onOk = () => {
    // console.log(this.formRef);
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const values = {
        ...fieldsValue
      };
      console.log('Received values of form: ', values);
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK({
          ...values
        });
      }
    }).catch();
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onHandlerCancel } = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("AddUserModal.render");
    const { colon, modalTitle, modalWidth, modalVisible, loading, sexData, statusData } = this.props;

    const formItemLayout = modalFormItemLayout;

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        maskClosable={false}
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label="用户姓名" name='userName' {...formItemLayout} colon={colon}
                rules={[
                  { max: 10, message: '用户姓名最多允许输入10个字符' },
                  { required: true, message: '用户姓名必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="用户账号" name='userCode' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '用户账号最多允许输入20个字符' },
                  { required: true, message: '用户账号必输' },
                  { pattern: new RegExp(/^[A-Za-z][A-Za-z0-9]{1,20}$/, "g"), message: '用户账号只能以字母开头，由A-Za-z0-9组成，最大长度为20' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="性别" name='sex' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '性别必输' }
                ]}
                initialValue={'0'}
              >
                <ASelect dataSource={sexData} />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="状态" name='userStates' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '状态必输' }
                ]}
                initialValue={'0'}
              >
                <ASelect dataSource={statusData} />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="电子邮箱" name='email' {...formItemLayout} colon={colon}
                rules={[
                  { max: 30, message: '电子邮箱最多允许输入30个字符' },
                  { required: true, message: '电子邮箱必输' },
                  { pattern: new RegExp(/^([a-zA-Z0-9_\.\-])+\@([a-zA-Z0-9\-])+(\.[a-zA-Z]{2,6}){1,3}$/, "g"), message: '电子邮箱格式不正确' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="手机号码" name='mobile' {...formItemLayout} colon={colon}
                rules={[
                  { max: 11, message: '手机号码最多允许输入11个字符' },
                  { required: true, message: '手机号码必输' },
                  { pattern: new RegExp(/^1[3578][0-9]{9}$/, "g"), message: '手机号码格式不正确' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}