import React, { useState, useEffect, Key } from 'react';
import { Modal, Drawer, message, Tag, Space } from 'antd';
import { connect, Dispatch } from 'umi';
import { USER_LIST,
  USER_ADD, USER_UPDATE,
  USER_DELETE, USER_ROLE,
  USER_ASSIGN_ROLE,
  USER_RESET_PASS,
  UPDATE_STATE } from '@/actions/user';
import ProCard from '@ant-design/pro-card';
import ProDescriptions, { ProDescriptionsItemProps } from '@ant-design/pro-descriptions';
import {PageContainer, FooterToolbar } from '@ant-design/pro-layout';
import AGrid, { AGridButtonCallBackModel } from '@/components/SelfComp/AGrid';
import AButton from '@/components/SelfComp/AButton';
import { ConnectState } from '@/models/connect';
import { DataItem } from '@/models/common';
import { getItemValue } from '@/utils/commons';
import UserQueryForm from './components/UserQueryForm';
import AddUserModal from './components/AddUserModal';
import UpdateUserModal from './components/UpdateUserModal';
import AssignRoleModal from './components/AssignRoleModal';
import { AssignRoleItem, AssignUserRoleData, UserItem } from './data';

interface UserListProps {
  dispatch: Dispatch;
  userList: UserItem[],
  total: number,
  loading: boolean;
  addLoading: boolean;
  updateLoading: boolean;
  deleteLoading: boolean;
  previewLoading: boolean;
  assignLoading: boolean;
  resetLoading: boolean;
  sexData: DataItem[];
  statusData: DataItem[];
}

const EMPTY_USER: UserItem = {
  userId: '',
  userCode: '',
  userName: '',
  userStates: '',
  sex: '',
  email: '',
  mobile: ''
}

const EMPTY_ASSIGN_ROLE: AssignUserRoleData = {
  userId: '',
  allRoleData: [],
  assignedRoleData: []
}

/**
 * 用户列表管理
 * @param props 属性
 * @returns 用户列表
 */
const UserList: React.FC<UserListProps> = (props) => {
  const code = 'user-list';
  const { userList, loading, total, addLoading, updateLoading, deleteLoading, assignLoading, sexData, statusData } = props;
  const [addModalVisible, handleAddModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [assignRoleModalVisible, handleAssignRoleModalVisible] = useState<boolean>(false);
  const [row, setRow] = useState<UserItem>();
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const [selectedRows, setSelectedRows] = useState<UserItem[]>([]);
  const [queryParam, setQueryParam] = useState<UserItem>(EMPTY_USER);
  const [pageSize, setPageSize] = useState<number>(10);
  const [userFormData, setUserFormData] = useState<UserItem>(EMPTY_USER);
  const [roleFormData, setRoleFormData] = useState<AssignUserRoleData>(EMPTY_ASSIGN_ROLE);
  const rowKey = (record: UserItem) => {
    return record.userId;
  }
  const pkField = 'userId';
  const columns = [
    {
      title: '用户姓名',
      dataIndex: 'userName',
      width: 120,
      render: (text: string, record: UserItem, index: number) => {
        return <a onClick={() => setRow(record)}>{text}</a>;
      },
    },
    {
      title: '用户账号',
      dataIndex: 'userCode',
      width: 150
    },
    {
      title: '性别',
      dataIndex: 'sex',
      width: 80
    },
    {
      title: '状态',
      dataIndex: 'userStates',
      width: 100,
      render: (text: string, record: UserItem, index: number) => userStatusFunc(record),
    },
    {
      title: '电子邮箱',
      dataIndex: 'email',
      width: 200
    },
    {
      title: '手机号码',
      dataIndex: 'mobile',
      width: 150
    }
  ];

  useEffect(() => {
    console.info('UserList.useEffect');
    // fetchAllUser(1, 10, queryParam);
    return () => {
      const { dispatch } = props;
      dispatch(UPDATE_STATE({
        rows: [],
        total: 0
      }));
    }
  }, []);

  const userStatusFunc = (record: UserItem) => {
    const { statusData } = props;
    const { userStates } = record;
    const value = getItemValue(statusData, userStates);
    if (userStates === '1') {
      return <Tag color='error'>{value}</Tag>;
    }
    return <Tag color='processing'>{value}</Tag>;
  }

  /**
   * 查询满足条件的数据字典
   */
  const handleQuery = (record: UserItem) => {
    console.info('UserList.handleQuery');
    console.info(record);
    const queryParam = {
      ...record
    };
    // 查询条件保存起来，方便点击页码时使用
    // setState回调方法，保证先更新state
    fetchAllUser(1, pageSize, queryParam);
  }

  const handleReset = () => {
    // 是否清空表格
  }

  /**
   * 分页查询系统参数信息
   * @param {*} pageNum 页码
   * @param {*} pageSize 每页记录条数
   */
  const fetchAllUser = (pageNum: number, pageSize: number, queryParam: UserItem) => {
    const { dispatch } = props;
    dispatch(USER_LIST({
      ...queryParam,
      pageSize,
      pageNum
    }));
  }

  /**
   * 删除用户，支持多笔删除
   */
  const deleteUsers = async () => {
    const { dispatch } = props;
    const userIds = [...selectedRowKeys]
    const res = await dispatch(USER_DELETE(userIds));
    if (res) {
      setSelectedRowKeys([]);
      setSelectedRows([]);
      dispatch(USER_LIST({
        ...queryParam,
        pageSize,
        pageNum: 1,
      }));
    }
  }

  /**
   * 处理按钮点击回调事件
   * @param {*} payload 数据包
   */
  const handleBtnCallBack = (callBackModel: AGridButtonCallBackModel<UserItem>) => {
    console.log('handleBtnCallBack');
    console.log(callBackModel);
    // btn 按钮
    // keys 表格勾选数组
    const { btn } = callBackModel;
    const { alias } = btn;
    // 新增
    if (alias === 'add') {
      handleAddModalVisible(true);
      return;
    }
    // 修改
    if (alias === 'edit') {
      const { rows } = callBackModel;
      setUserFormData(rows[0]);
      handleUpdateModalVisible(true);
      return;
    }
    // 删除
    if (alias === 'delete') {
      // 调用删除服务，删除勾选数据
      const { keys } = callBackModel;
      const userId= keys[0];
      deleteUser(userId);
      return;
    }
  }

  /**
   * 删除用户，暂时只支持单笔删除
   * @param userId 用户id
   */
  const deleteUser = async (userId: Key) => {
    const { dispatch } = props;
    const userIds = [userId]
    const res = await dispatch(USER_DELETE(userIds));
    if (res) {
      dispatch(USER_LIST({
        ...queryParam,
        pageSize,
        pageNum: 1,
      }));
    }
  }

  /**
   * 表格勾选回调函数
   * @param {*} rows 表格选中数据集合
   */
  const onSelectRow = (keys: React.Key[], rows: UserItem[]) => {
    setSelectedRowKeys(keys);
    setSelectedRows(rows);
  };

  /**
   * 页码改变的回调，参数是改变后的页码及每页条数
   * @param page 改变后的页码，上送服务器端
   * @param pageSize 每页数据条数
   */
  const onPageNumAndSizeChange = (page: number, pageSize: number) => {
    console.log(page, pageSize);
    setPageSize(pageSize);
    fetchAllUser(page, pageSize, queryParam);
  };

  const renderLeftButton = () => {
    const { previewLoading, resetLoading } = props;
    return (
      <>
        <AButton
          type='primary'
          code='assignRole'
          pageCode={code}
          name='分配角色'
          loading={previewLoading}
          onClick={openAssignRoleModal} />
        <AButton
          danger
          code='resetPass'
          pageCode={code}
          name='重置密码'
          loading={resetLoading}
          onClick={handleResetUserPassword} />
      </>
    );
  }

  /**
   * 角色授权，打开角色授权窗口
   */
  const openAssignRoleModal = async () => {
    // 表格选中内容
    console.info('openAssignRoleModal');
    if (selectedRowKeys.length === 0) {
      message.warn('请先选择一条数据!');
      return;
    }
    if (selectedRowKeys.length > 1) {
      message.warn('只能选择一条数据!');
      return;
    }
    // 获取用户id
    const userId = selectedRowKeys[0];
    console.info('userId', userId);
    const { dispatch } = props;
    const res = await dispatch(USER_ROLE({
      userId
    }));
    if (res) {
      // 查询菜单权限树，并根据勾选角色ID对应已分配的权限
      // 这里试试dispatch的回调方法
      // 只有查询成功才会弹窗
      const { result } = res;
      const { allRoleData, assignedRoleData } = result;
      const roleFormData = {
        userId,
        allRoleData,
        assignedRoleData
      };
      setRoleFormData(roleFormData);
      handleAssignRoleModalVisible(true);
    }
  }

  const handleAssignModal = async (record: AssignRoleItem) => {
    console.log('handleAssignModalOk');
    console.log(record);
    const { dispatch } = props;
    const res = await dispatch(USER_ASSIGN_ROLE(record));
    if (res) {
      setRoleFormData(EMPTY_ASSIGN_ROLE);
      handleAssignRoleModalVisible(false);
    }
  }

  const handleAssignModalCancel = () => {
    setRoleFormData(EMPTY_ASSIGN_ROLE);
    handleAssignRoleModalVisible(false);
  }

  const handleResetUserPassword = () => {
    if (selectedRowKeys.length <= 0) {
      message.warn('请至少选择一条数据!');
      return;
    }
    Modal.confirm({
      title: '重置确认',
      content: '确定重置选中用户密码？',
      okText: '确定',
      okType: 'danger',
      cancelText: '取消',
      onOk: () => resetUserPassword(selectedRowKeys),
      onCancel() {},
    });
    return;
  }

  const resetUserPassword = async (rows: React.Key[]) => {
    console.log('resetUserPassword');
    const { dispatch } = props;
    const res = await dispatch(USER_RESET_PASS(rows));
    console.log(res);
    if (res) {
      message.success('用户密码重置成功');
    }
  }

  const handleAddModal = async (record: UserItem) => {
    console.log(record);
    const { dispatch } = props;
    console.info(dispatch);
    const res = await dispatch(USER_ADD(record));
    console.info(res);
    if (res) {
      handleAddModalVisible(false);
      fetchAllUser(1, pageSize, queryParam);
    }
  }

  const handleAddModalCancel = () => {
    handleAddModalVisible(false);
  };

  /**
   * 更新面板的确定事件
   * @param {*} record 更新表单
   */
  const handleUpdateModal = async (record: UserItem) => {
    console.log('handleUpdateModalOk');
    console.log(record);
    console.info(userFormData);
    const { dispatch } = props;
    const res = await dispatch(USER_UPDATE(record));
    console.info(res);
    if (res) {
      handleUpdateModalVisible(false);
      setUserFormData(EMPTY_USER);
      fetchAllUser(1, pageSize, queryParam);
    }
  }

  const handleUpdateModalCancel = () => {
    handleUpdateModalVisible(false);
    setUserFormData(EMPTY_USER);
  };

  return (
    <PageContainer>
      <Space direction='vertical' size='middle' style={{ display: 'flex' }}>
        <ProCard title="用户列表" headerBordered>
          <UserQueryForm
            colon={false}
            loading={loading}
            onSubmit={(record) => {
              setQueryParam(record);
              handleQuery(record);
            }}
            onReset={handleReset}
          />
        </ProCard>
        <ProCard>
          <AGrid
            code={code}
            btnCallBack={handleBtnCallBack}
            renderLeftButton={renderLeftButton}
            columns={columns}
            rowKey={rowKey}
            pkField={pkField}
            dataSource={userList}
            loading={loading}
            total={total}
            onSelectRow={onSelectRow}
            onPageNumAndSizeChange={onPageNumAndSizeChange}
          />
        </ProCard>
      </Space>
      {selectedRows?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              已选择{' '}
              <a style={{ fontWeight: 600 }}>{selectedRows.length}</a>{' '}
              项
              &nbsp;&nbsp;
            </div>
          }
        >
          <AButton
            type='primary'
            code='batchDel'
            pageCode={code}
            name='批量删除'
            loading={deleteLoading}
            onClick={deleteUsers}
          />
        </FooterToolbar>
      )}
      {
        !addModalVisible ? null :
        <AddUserModal
          colon={false}
          modalTitle='新增用户'
          modalWidth={1000}
          modalVisible={addModalVisible}
          loading={addLoading}
          sexData={sexData}
          statusData={statusData}
          onHandlerOK={handleAddModal}
          onHandlerCancel={handleAddModalCancel}
        />
      }
      {
        !updateModalVisible ? null :
        <UpdateUserModal
          colon={false}
          modalTitle=''
          modalWidth={1000}
          modalVisible={updateModalVisible}
          flag='edit'
          loading={updateLoading}
          formData={userFormData}
          sexData={sexData}
          statusData={statusData}
          onHandlerOK={handleUpdateModal}
          onHandlerCancel={handleUpdateModalCancel}
        />
      }
      {
        !assignRoleModalVisible ? null :
        <AssignRoleModal
          colon={false}
          modalTitle='分配角色'
          modalWidth={450}
          modalVisible={assignRoleModalVisible}
          loading={assignLoading}
          formData={roleFormData}
          onHandlerOK={handleAssignModal}
          onHandlerCancel={handleAssignModalCancel}
        />
      }
      <Drawer
        width={600}
        open={!!row}
        onClose={() => {
          setRow(undefined);
        }}
        closable={false}
      >
        {row?.userId && (
          <ProDescriptions<UserItem>
            column={2}
            title={row?.userName}
            request={async () => ({
              data: row || {},
            })}
            params={{
              id: row?.userId,
            }}
            columns={columns as ProDescriptionsItemProps<UserItem>[]}
          />
        )}
      </Drawer>
    </PageContainer>
  );
};


export default connect((state: ConnectState) => ({
  userList: state.users.rows,
  roleList: state.roles.rows,
  total: state.users.total,
  sexData: state.systems.sexData,
  statusData: state.systems.statusData,
  loading: state.loading.effects['users/fetchAllUser'],
  addLoading: state.loading.effects['users/addUser'],
  updateLoading: state.loading.effects['users/updateUser'],
  deleteLoading: state.loading.effects['users/deleteUsers'],
  previewLoading: state.loading.effects['users/fetchRolesByUser'],
  assignLoading: state.loading.effects['users/assignRolesToUser'],
}))(UserList);