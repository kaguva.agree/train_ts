import React from 'react';
import { Modal, Form, Row, Col, Input, TreeSelect, InputNumber, Radio } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import ASelect, { ASelectOptionType } from '@/components/SelfComp/ASelect';
import { formLayout, modalFormItemLayout, maxModalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { DataItem } from '@/models/common';
import { MenuItem, MenuSelectTreeItem } from '../data';

type AddMenuProps<T> = FormModalProps<T> & {
  menuTypeData: DataItem[];
  menuActionData: DataItem[];
  statusData?: DataItem[];
  menuTreeData: MenuSelectTreeItem[];
  hideInMenuData: DataItem[];
}
type AddMenuPropsEx<T> = Omit<AddMenuProps<T>, 'formData'>;

type AddMenuState = {
  /** 按钮功能栏位是否可见，true-类型为按钮，false-类型为菜单 */
  btnActionVisible: boolean;
  /** 菜单是否隐藏栏位是否可见 */
  hideInMenuVisible: boolean;
  /** 路由地址栏位是否可见，true-显示，false-不显示 */
  routeAddVisible: boolean;
}

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

export default class AddMenuModal extends React.PureComponent<AddMenuPropsEx<MenuItem>, AddMenuState> {

  formRef = React.createRef<FormInstance>();

  state: AddMenuState = {
    btnActionVisible: false,
    hideInMenuVisible: true,
    routeAddVisible: true,
  }

  componentDidMount() {
    console.info('AddMenuModal.componentDidMount');
  }

  componentWillUnmount(): void {
    console.info('AddMenuModal.componentWillUnmount');
  }

  onOk = () => {
    this.formRef.current!.validateFields().then(fieldsValue => {
      console.info(fieldsValue);
      const values = {
        ...fieldsValue
      };
      console.log('Received values of form: ', values);
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK({
          ...values
        });
      }
    }).catch();
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const { onHandlerCancel } = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  afterClose = () => {
    console.info('AddMenuModal.afterClose');
    this.setState({
      btnActionVisible: false,
      hideInMenuVisible: true,
      routeAddVisible: true,
    });
  }

  /**
   * 菜单类型栏位改变事件
   * 默认情况下 onChange 里只能拿到 value
   */
  menuTypeChange = (value: string, option: ASelectOptionType | ASelectOptionType[]) => {
    // 查询该功能码对应的交易信息
    console.log('menuTypeChange');
    // 菜单类型为2-按钮时，显示按钮位置栏位
    if (value === '2') {
      this.setState({
        btnActionVisible: true,
        hideInMenuVisible: false,
        routeAddVisible: false,
      });
      return;
    }
    if (value === '1') {
      this.setState({
        btnActionVisible: false,
        hideInMenuVisible: true,
        routeAddVisible: true,
      });
      return;
    }
    if (value === '3') {
      this.setState({
        btnActionVisible: false,
        hideInMenuVisible: true,
        routeAddVisible: true,
      });
      return;
    }
    this.setState({
      btnActionVisible: false,
      routeAddVisible: true,
    });
    this.formRef.current!.resetFields([
      'menuAction'
    ]);
  }

  render() {
    console.info("AddMenuModal.render");
    const { colon, modalTitle, modalWidth, modalVisible, menuTypeData, menuActionData, menuTreeData, loading } = this.props;
    const { hideInMenuData } = this.props;
    const { btnActionVisible, hideInMenuVisible, routeAddVisible } = this.state;
    const formItemLayout = modalFormItemLayout;

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        afterClose={this.afterClose}
        maskClosable={false}
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={24}>
              <FormItem label="上级菜单" name='parentMenuId' {...maxModalFormItemLayout} colon={colon}>
                <TreeSelect
                  dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                  treeData={menuTreeData}
                  allowClear
                  showSearch
                  treeNodeFilterProp="title"
                  placeholder="请选择"
                />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="菜单类型" name='menuType' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '菜单类型必输' }
                ]}
                initialValue={'1'}
              >
                <ASelect dataSource={menuTypeData} onChange={this.menuTypeChange}/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="菜单名称" name='menuName' {...formItemLayout} colon={colon}
                rules={[
                  { max: 10, message: '菜单名称最多允许输入10个字符' },
                  { required: true, message: '菜单名称必输' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          {
            !routeAddVisible ? null :
            <Row>
              <Col span={24}>
                <FormItem label="路由地址" name='menuPath' {...maxModalFormItemLayout} colon={colon}
                  rules={[
                    { required: routeAddVisible, message: '路由地址必输' },
                    { max: 50, message: '路由地址最多允许输入50个字符' },
                    { pattern: new RegExp(/^\/[A-Za-z][A-Za-z0-9\/-]{1,50}$/, "g"), message: '路由地址只能以/字母开头，由A-Za-z0-9组成，最大长度为50' }
                  ]}
                >
                  <Input />
                </FormItem>
              </Col>
            </Row>
          }
          <Row>
            <Col span={12}>
              <FormItem label="菜单图标" name='menuIcon' {...formItemLayout} colon={colon}>
                <Input />
              </FormItem>
            </Col>
            {
              !btnActionVisible ? null :
              <Col span={12}>
                <FormItem label="按钮功能" name='menuAction' {...formItemLayout} colon={colon}
                  rules={[
                    { required: btnActionVisible, message: '按钮功能必输' }
                  ]}
                >
                  <ASelect dataSource={menuActionData}/>
                </FormItem>
              </Col>
            }
            {
              !hideInMenuVisible ? null :
              <Col span={12}>
                <FormItem label="菜单是否显示" name='hideInMenu' {...formItemLayout} colon={colon}
                  tooltip='选择隐藏则菜单不会出现在左侧菜单栏，但仍可以访问'
                  rules={[
                    { required: hideInMenuVisible, message: '菜单是否隐藏必输' }
                  ]}
                >
                  <ASelect dataSource={hideInMenuData}/>
                </FormItem>
              </Col>
            }
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="菜单编号" name='menuCode' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '菜单编号最多允许输入20个字符' },
                  { required: true, message: '菜单编号必输' },
                  { pattern: new RegExp(/^[A-Za-z][A-Za-z0-9-_:]{1,20}$/, "g"), message: '菜单编号只能以字母开头，由A-Za-z0-9组成，最大长度为20' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="菜单别名" name='menuAlias' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '菜单别名最多允许输入20个字符' },
                  { required: true, message: '菜单别名必输' },
                  { pattern: new RegExp(/^[A-Za-z][A-Za-z0-9-_]{1,20}$/, "g"), message: '菜单别名只能以字母开头，由A-Za-z0-9组成，最大长度为20' }
                ]}
              >
                <Input />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="显示排序" name='menuSort' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '显示排序必输' }
                ]}
              >
                <InputNumber min={0} style={{ width: '100%' }}/>
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem label="新窗口打开" name='newOpen' {...formItemLayout} colon={colon}
                rules={[
                  { required: true, message: '新窗口打开必输' }
                ]}
                initialValue='1'
              >
                <RadioGroup name="newOpen">
                  <Radio value={'1'}>否</Radio>
                  <Radio value={'2'}>是</Radio>
                </RadioGroup>
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}