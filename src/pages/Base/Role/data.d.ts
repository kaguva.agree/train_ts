/**
 * 角色
 */
export interface RoleItem {
  /** 角色id */
  roleId: string;
  /** 角色编码 */
  roleCode: string;
  /** 角色名称 */
  roleName: string;
  /** 角色描述 */
  roleDesc: string;
  roleStatus?: string;
}

/**
 * 角色授权菜单
 */
export interface GrantAuthItem {
  /** 角色id */
  roleId: string;
  /** 菜单id集合 */
  menuIds: string[];
}

/**
 * 菜单树节点
 */
export interface MenuTreeItem {
  key: string;
  title: string;
  parentId: string;
  children?: MenuTreeItem[];
}

/**
 * 菜单树选中节点
 */
export interface GrantKey {
  /** 菜单id */
  id: string;
  /** 父菜单id */
  pId: string;
}

/**
 * 授权窗口所需值
 */
export interface AuthTreeData {
  roleId: React.Key;
  grantKeys: GrantKey[];
  menuTree: MenuTreeItem[];
}