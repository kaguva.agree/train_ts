import React from 'react';
import { Form, Input } from 'antd';
import { basicFormItemLangLayout } from '@/utils/commons';
import QueryFilterForm from '@/components/SelfComp/QueryFilterForm';
import { QueryFormProps } from '@/models/FormModal';
import { RoleItem } from '../data';

const FormItem = Form.Item;

export type MenuQueryProps = QueryFormProps<RoleItem> & {
}

class RoleQueryForm extends React.PureComponent<MenuQueryProps> {

  render() {
    const { colon, loading, onSubmit, onReset } = this.props;
    const formItemLayout = basicFormItemLangLayout;

    return (
      <QueryFilterForm
        colon={colon}
        loading={loading}
        onSubmit={onSubmit}
        onReset={onReset}
      >
        <FormItem label="角色编码" name='roleCode' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
        <FormItem label="角色名称" name='roleName' {...formItemLayout} colon={colon}>
          <Input />
        </FormItem>
      </QueryFilterForm>
    );
  }
}

export default RoleQueryForm;