import React from 'react';
import { Modal, Form, Row, Col, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import { formLayout, modalFormItemLayout, maxModalFormItemLayout } from '@/utils/commons';
import { FormModalProps } from '@/models/FormModal';
import { FlagEnum } from '@/models/common';
import { RoleItem } from '../data';

type UpdateRoleProps<T> = FormModalProps<T> & {
  flag: FlagEnum;
}

const FormItem = Form.Item;
const TextArea = Input.TextArea;

export default class UpdateRoleModal extends React.PureComponent<UpdateRoleProps<RoleItem>> {

  formRef = React.createRef<FormInstance>();

  componentDidMount() {
    console.info('UpdateRoleModal.componentDidMount');
  }

  onOk = () => {
    const { formData, flag } = this.props;
    if (flag === 'view') {
      const { onHandlerOK } = this.props;
      if (onHandlerOK) {
        onHandlerOK(formData);
      }
    } else {
      const { roleId } = formData;
      console.log(this.formRef);
      this.formRef.current!.validateFields().then(fieldsValue => {
        console.info(fieldsValue);
        const values = {
          ...fieldsValue,
          roleId
        };
        console.log('Received values of form: ', values);
        const { onHandlerOK } = this.props;
        if (onHandlerOK) {
          onHandlerOK({
            ...values
          });
        }
      }).catch((err) => console.info('表单校验不通过'));
    }
  };

  onCancel = () => {
    // 先清除form表单
    this.formRef.current!.resetFields();
    const {onHandlerCancel} = this.props;
    if (onHandlerCancel) {
      onHandlerCancel();
    }
  }

  render() {
    console.info("UpdateRoleModal.render");
    const { formData, colon, modalWidth, modalVisible, loading, flag } = this.props;
    console.info(`flag: ${flag}`);
    const formItemLayout = modalFormItemLayout;
    const modalTitle = (flag === 'edit' ? '修改角色' : '查看角色信息');
    const disabled = (flag === 'edit' ? false : true);

    return (
      <Modal
        title={modalTitle}
        destroyOnClose
        width={modalWidth}
        open={modalVisible}
        confirmLoading={loading}
        onOk={this.onOk}
        onCancel={this.onCancel}>
        <Form layout={formLayout} ref={this.formRef}>
          <Row>
            <Col span={12}>
              <FormItem label="角色编码" name='roleCode' {...formItemLayout} colon={colon}
                rules={[
                  { max: 10, message: '角色编码最多允许输入10个字符' },
                  { required: true, message: '角色编码必输' },
                  { pattern: new RegExp(/^[A-Za-z][A-Za-z0-9]{1,50}$/, "g"), message: '角色编码只能以字母开头，由A-Za-z0-9组成，最大长度为10' }
                ]}
                initialValue={formData.roleCode}
              >
                <Input disabled />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <FormItem label="角色名称" name='roleName' {...formItemLayout} colon={colon}
                rules={[
                  { max: 20, message: '角色名称最多允许输入20个字符' },
                  { required: true, message: '角色名称必输' },
                ]}
                initialValue={formData.roleName}
              >
                <Input disabled={disabled} />
              </FormItem>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <FormItem label="角色描述" name='roleDesc' {...maxModalFormItemLayout} colon={colon}
                rules={[
                  { max: 30, message: '角色描述最多允许输入30个字符' },
                  { required: true, message: '角色描述' }
                ]}
                initialValue={formData.roleDesc}
              >
                <TextArea disabled={disabled} maxLength={30} showCount />
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    );
  }
}