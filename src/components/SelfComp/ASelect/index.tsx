import React from 'react';
import { Select, SelectProps } from 'antd';
import { DefaultOptionType } from 'antd/lib/select';
import { createSelectOptions } from '@/utils/commons';
import { DataItem } from '@/models/common';

export interface ASelectValueType {
  value: string;
  label: string;
}

export interface ASelectOptionType extends DefaultOptionType {
}

export type ASelectProps =  SelectProps<string, ASelectOptionType>  & {
  /** 下拉框数据源 */
  dataSource: DataItem[];
  /** 下拉框样式，ui显示形式，1:key-value，通过onChange获取到的value为key，2:value，3:key-value，onChange获取到的value为value */
  type: '1' | '2' | '3' | '4';
}

/**
 * 下拉框组件的简单封装，设置了一些默认属性
 * dataSource 数据源
 * type ui显示形式，1:key-value，通过onChange获取到的value为key，2:value，3:key-value，onChange获取到的value为value
 */
export default class ASelect extends React.PureComponent<ASelectProps> {

  static defaultProps = {
    showSearch: true,
    allowClear: true,
    type: '1',
    placeholder: '请选择',
    disabled: false
  };

  onHandleChange = (value: string, option: ASelectOptionType | ASelectOptionType[]) => {
    // 必须暴漏onChange方法，否则Form表单无法获取组件的值
    const { onChange } = this.props;
    if (onChange) {
      onChange(value, option);
    }
  }

  render() {

    // 获取 props 中的属性，将需要透传给 Select 组件的属性放在 rest 中
    const { dataSource, type, showSearch, allowClear, placeholder, disabled, value, ...rest } = this.props;
    const options = createSelectOptions(dataSource, type);

    return (
      <Select
        {...rest}
        showSearch={showSearch}
        allowClear={allowClear}
        placeholder={placeholder}
        disabled={disabled}
        onChange={this.onHandleChange}
        defaultValue={value}
        value={value}
        options={options}
      />
    );
  }
}