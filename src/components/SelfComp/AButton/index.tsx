import React from 'react';
import { Button, ButtonProps } from 'antd';
import { hasButtonPermission } from '@/utils/authority';

export type AButtonProps =  ButtonProps  & {
  /** 页面代码 */
  pageCode: string;
  /** 按钮代码 */
  code: string;
  /** 按钮名称 */
  name: string;
}

/**
 * 按钮高阶组件，对权限的简单封装，设置了一些默认属性
 * 属性如下：
 * pageCode-页面代码<br/>
 * code-按钮代码<br/>
 * name-按钮名称<br/>
 * onClick-按钮点击事件
 */
class AButton extends React.PureComponent<AButtonProps> {

  static defaultProps = {
    type: 'primary'
  };

  render() {
    // pageCode-页面代码
    // type-按钮类型
    // code-按钮代码
    // name-按钮名称
    // onClick-按钮点击事件
    const { pageCode, type, code, name, onClick, loading, ...res } = this.props;
    // 判断code是否在权限列表中
    const hasPermission = hasButtonPermission(pageCode, code);
    console.info(`AButton, pageCode: ${pageCode}, code: ${code}, ${hasPermission}`);

    return (
      <React.Fragment>
        {
          !hasPermission ? null :
          <Button
            {...res}
            type={type}
            onClick={onClick}
            loading={loading}
          >
            {name}
          </Button>
        }
      </React.Fragment>
    );
  }
}

export default AButton;