import React from 'react';
import { Row, Col, Checkbox } from 'antd';
import { CheckboxValueType } from 'antd/lib/checkbox/Group';
import memoizeOne from 'memoize-one';
import { InputSpecifiedProps, InputSpecifiedState } from '../data';

const currentYear = new Date().getUTCFullYear();
/**
 * cron时间表达式生成器-月面板
 * 属性如下：
 */
class InputSpecified extends React.PureComponent<InputSpecifiedProps, InputSpecifiedState> {

  state: InputSpecifiedState = {
    selected: []
  }

  /**
   * 静态函数，根据新传入的props来映射到state。
   * 该函数必须有返回值。当props传入的内容不需要影响state，就必须返回一个null
   */
  static getDerivedStateFromProps(nextProps: InputSpecifiedProps, prevState: InputSpecifiedState) {
    // console.log('InputFromTo.getDerivedStateFromProps: ', nextProps, prevState);
    // 重新查询清空表格勾选
    const { disabled, value } = nextProps;
    if (disabled) {
      return {
        selected: []
      };
    } else {
      if (value) {
        const selected = value.split(',').map((v) => parseInt(v, 10));
        return {
          selected
        };
      }
    }
    return null;
  }

  /**
   * 创建多选框集合
   * @param {*} disabled 栏位是否可用
   * @returns 多选框集合
   */
  createCheckbox = (disabled: boolean) => {
    console.log('年createCheckbox');
    const checks = [];
    for (let i = currentYear - 6; i < currentYear + 30; i++) {
      checks.push(
        <Col key={i} span={2}>
          <Checkbox disabled={disabled} value={i}>
            {i}
          </Checkbox>
        </Col>,
      );
    }
    return checks;
  }
  // 生成可缓存的方法，当disabled不变时，不计算直接返回上一次的结果
  memoizedCreateCheckbox = memoizeOne(this.createCheckbox);

  onChangeSelected = (value: Array<CheckboxValueType>) => {
    const { onChange } = this.props;
    // 当没有勾选时，默认选中0
    if (onChange) {
      onChange(value.length === 0 ? `${currentYear}` : value.join(','));
    }
  }

  render() {
    const { disabled, value } = this.props;
    let { selected } = this.state;
    console.log(selected, value);
    const checkList = this.memoizedCreateCheckbox(disabled);

    return (
      <React.Fragment>
        指定年
        <br />
        <Checkbox.Group style={{ width: '100%' }} value={selected} onChange={this.onChangeSelected}>
            <Row>{checkList}</Row>
        </Checkbox.Group>
      </React.Fragment>
    );
  }

}

export default InputSpecified;