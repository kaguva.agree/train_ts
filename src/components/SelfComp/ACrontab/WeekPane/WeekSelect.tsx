import React from 'react';
import { Select, SelectProps } from 'antd';
import memoizeOne from 'memoize-one';

export const weekOptions = {
  MON: '星期一',
  TUE: '星期二',
  WED: '星期三',
  THU: '星期四',
  FRI: '星期五',
  SAT: '星期六',
  SUN: '星期日',
} as const;

export type WeekSelectProps =  SelectProps<string>  & {
}

class WeekSelect extends React.PureComponent<WeekSelectProps> {

  /**
   * 创建星期的下拉框选项
   * @returns 星期的下拉框选项集合
   */
  createOptions = () => {
    console.log('周createCheckbox');
    return Object.entries(weekOptions).map(([weekCode, weekName]) => {
      return (
        {
          value: weekCode,
          label: weekName
        }
      );
    })
  }
  // 生成可缓存的方法，当disabled不变时，不计算直接返回上一次的结果
  memoizedCreateOptions = memoizeOne(this.createOptions);

  render() {
    const weekSelectOptions = this.memoizedCreateOptions();
    console.log(weekSelectOptions);

    return (
      <Select
        options={weekSelectOptions}
        onClick={(e) => e.preventDefault()}
        {...this.props}
      />
    );
  }
}

export default WeekSelect;