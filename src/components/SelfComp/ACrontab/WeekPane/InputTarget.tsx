import React, { Fragment, PureComponent } from 'react';
import { InputNumber, Select } from 'antd';
import memoizeOne from 'memoize-one';
import WeekSelect from './WeekSelect';
import { InputTargetProps, InputTargetState } from '../data';

/**
 * cron时间表达式生成器-周面板
 * 属性如下：
 */
class InputTarget extends React.PureComponent<InputTargetProps, InputTargetState > {

  state: InputTargetState = {
    weekOfMonth: 1,
    dayOfWeek: 'MON'
  }

  /**
   * 静态函数，根据新传入的props来映射到state。
   * 该函数必须有返回值。当props传入的内容不需要影响state，就必须返回一个null
   */
  static getDerivedStateFromProps(nextProps: InputTargetProps, prevState: InputTargetState) {
    // console.log('InputFromTo.getDerivedStateFromProps: ', nextProps, prevState);
    // 重新查询清空表格勾选
    const { disabled } = nextProps;
    if (disabled) {
      return {
        weekOfMonth: 1,
        dayOfWeek: 'MON'
      };
    }
    return null;
  }

  onChangeWeekOfMonth = (value: number | null) => {
    const { onChange } = this.props;
    const { dayOfWeek } = this.state;
    if (value) {
      this.setState({
        weekOfMonth: value
      });
      if (onChange) {
        onChange(`${value || 1}#${dayOfWeek}`);
      }
    }
  }

  onChangeDayOfWeek = (value: string) => {
    const { onChange } = this.props;
    const { weekOfMonth } = this.state;
    this.setState({
      dayOfWeek: value
    });
    if (onChange) {
      onChange(`${weekOfMonth}#${value || 'MON'}`);
    }
  }

  render() {
    const { disabled, value } = this.props;
    let { weekOfMonth, dayOfWeek } = this.state;
    if (!disabled && value) {
      const [weekOfMonthEx, dayOfWeek] = value.split('#');
      weekOfMonth = parseInt(weekOfMonthEx, 10);
      console.log('周循环', weekOfMonth, dayOfWeek, value);
    }

    return (
      <React.Fragment>
        本月第&nbsp;
        <InputNumber
            disabled={disabled}
            min={1}
            max={5}
            value={weekOfMonth}
            onChange={this.onChangeWeekOfMonth}
            style={{ width: 100 }}
        />
        &nbsp;周的&nbsp;
        <WeekSelect
          disabled={disabled}
          value={dayOfWeek}
          onChange={this.onChangeDayOfWeek}
          style={{ width: 100 }}
        />
        &nbsp;执行一次
      </React.Fragment>
    );
  }
}

export default InputTarget;