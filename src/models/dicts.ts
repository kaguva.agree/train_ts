import { Effect, Reducer } from 'umi';
import { message } from 'antd';
import { DictItem } from '@/pages/System/Dict/data';
import { fetchAllDict, addDict, updateDict, deleteDicts } from '@/services/dictApis';
import { DataItem } from './common';
import { TradeResult } from './connect';

export interface DictModelState {
  rows?: DictItem[];
  total?: number;
  pageSize?: number;
  editFlagData?: DataItem[],
  statusData?: DataItem[],
}

export interface DictModelType {
  namespace: 'dicts';
  state: DictModelState;
  effects: {
    fetchAllDict: Effect;
    addDict: Effect;
    updateDict: Effect;
    deleteDicts: Effect;
  };
  reducers: {
    updateState: Reducer<DictModelState>;
  };
}

const DictModel: DictModelType = {
  namespace: 'dicts',

  state: {
    rows: [],
    total: 0,
    pageSize: 10,
    editFlagData: [
      {
        key: '0',
        value: '可编辑'
      },
      {
        key: '1',
        value: '不可编辑'
      }
    ],
    statusData: [
      {
        key: '0',
        value: '生效'
      },
      {
        key: '1',
        value: '失效'
      }
    ],
  },

  effects: {
    *fetchAllDict({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(fetchAllDict, payload);
        const { success } = response;
        if (success) {
          const { result } = response;
          yield put({ type: 'updateState', payload: result });
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败');
      }
    },
    *addDict({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(addDict, payload);
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('新增失败');
        return false;
      }
    },
    *updateDict({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(updateDict, payload);
        const { success } = response;
        if (success) {
          message.success('更新成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('更新失败');
        return false;
      }
    },
    *deleteDicts({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(deleteDicts, payload);
        const { success } = response;
        if (success) {
          message.success('删除成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('删除失败');
        return false;
      }
    },
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default DictModel;