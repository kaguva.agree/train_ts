import { BaseModelType, DataItem } from './common';

export interface SystemModelState {
  systemData?: DataItem[];
  clusterData?: DataItem[];
  statusData?: DataItem[];
  prdDateData?: DataItem[];
  sexData?: DataItem[];
  jobStatusData?: DataItem[];
  jobConcurrentData?: DataItem[];
  jobMisfirePolicyData?: DataItem[];
  taskTypeData: DataItem[];
  taskStatusData: DataItem[];
  cqStatusData: DataItem[];
  cqUndoTaskTypeData: DataItem[];
  tradeUndoTaskTypeData: DataItem[];
  tradeTaskNameData: DataItem[];
}

export interface SystemModelType extends BaseModelType {
  namespace: 'systems';
  state: SystemModelState;
}

const SystemModel: SystemModelType = {
  namespace: 'systems',

  state: {
    systemData: [],
    clusterData: [],
    statusData: [
      {
        key: '0',
        value: '生效'
      },
      {
        key: '1',
        value: '失效'
      }
    ],
    prdDateData: [],
    sexData: [
      {
        key: '0',
        value: '男'
      },
      {
        key: '1',
        value: '女'
      }
    ],
    jobStatusData: [],
    jobConcurrentData: [],
    jobMisfirePolicyData: [],
    taskTypeData: [],
    taskStatusData: [],
    cqStatusData: [],
    cqUndoTaskTypeData: [
      {
        key: '1',
        value: '录入需求详细信息'
      },
      {
        key: '2',
        value: '录入文档信息'
      },
      {
        key: '3',
        value: '分配交易'
      }
    ],
    tradeUndoTaskTypeData: [
      {
        key: '1',
        value: '确认任务'
      },
      {
        key: '2',
        value: '确认测试'
      },
      {
        key: '3',
        value: '确认审查'
      },
      {
        key: '4',
        value: '确认设计'
      },
    ],
    tradeTaskNameData: []
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default SystemModel;