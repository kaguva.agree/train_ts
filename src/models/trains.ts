import { Effect, Reducer } from 'umi';
import { message } from 'antd';
import { TrainItem } from '@/pages/Urban/Train/data';
import { fetchAllTrain, addTrain, updateTrain, deleteTrains } from '@/pages/Urban/Train/service';
import { DataItem } from './common';
import { TradeResult } from './connect';

export interface TrainModelState {
  rows?: TrainItem[];
  total?: number;
  statusData?: DataItem[],
}

export interface TrainModelType {
  namespace: 'trains';
  state: TrainModelState;
  effects: {
    fetchAllTrain: Effect;
    addTrain: Effect;
    updateTrain: Effect;
    deleteTrains: Effect;
  };
  reducers: {
    updateState: Reducer<TrainModelState>;
  };
}

const TrainModel: TrainModelType = {
  namespace: 'trains',

  state: {
    rows: [],
    total: 0,
    statusData: [
      {
        key: '0',
        value: '正常'
      },
      {
        key: '1',
        value: '异常'
      }
    ],
  },

  effects: {
    *fetchAllTrain({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(fetchAllTrain, payload);
        const { success } = response;
        if (success) {
          const { result } = response;
          yield put({ type: 'updateState', payload: result });
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败');
      }
    },
    *addTrain({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(addTrain, payload);
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('新增失败');
        return false;
      }
    },
    *updateTrain({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(updateTrain, payload);
        const { success } = response;
        if (success) {
          message.success('更新成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('更新失败');
        return false;
      }
    },
    *deleteTrains({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(deleteTrains, payload);
        const { success } = response;
        if (success) {
          message.success('删除成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('删除失败');
        return false;
      }
    },
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default TrainModel;