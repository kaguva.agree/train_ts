import { Effect, Reducer } from 'umi';
import { message } from 'antd';
import { LineOpeItem } from '@/pages/Urban/LineOpe/data';
import { fetchAllLineOpe, addLineOpe, updateLineOpe, deleteLineOpes } from '@/pages/Urban/LineOpe/service';
import { fetchAllStationForSelect } from '@/pages/Urban/Station/service';
import { fetchAllLineForSelect } from '@/pages/Urban/Line/service';
import { querySysDict } from '@/services/systemApis';
import { DataItem } from './common';
import { TradeResult } from './connect';

export interface LineOpeModelState {
  rows?: LineOpeItem[];
  total?: number;
  lineData?: DataItem[],
  directionData?: DataItem[],
  stationData?: DataItem[],
  statusData?: DataItem[],
}

export interface LineOpeModelType {
  namespace: 'lineopes';
  state: LineOpeModelState;
  effects: {
    fetchLineOpeInitParams: Effect;
    fetchAllLineOpe: Effect;
    addLineOpe: Effect;
    updateLineOpe: Effect;
    deleteLineOpes: Effect;
  };
  reducers: {
    updateState: Reducer<LineOpeModelState>;
  };
}

const LineOpeModel: LineOpeModelType = {
  namespace: 'lineopes',

  state: {
    rows: [],
    total: 0,
    lineData: [],
    directionData: [],
    stationData: [],
    statusData: [
      {
        key: '0',
        value: '正常'
      },
      {
        key: '1',
        value: '异常'
      }
    ],
  },

  effects: {
    *fetchLineOpeInitParams({ payload }, sagaEffects) {
      const { all, call, put } = sagaEffects;
      try {
        const [ lineResponse, stationResponse, directionResponse ] = yield all([
          call(fetchAllLineForSelect, {}),
          call(fetchAllStationForSelect, {}),
          call(querySysDict, {
            dictName: 'direction'
          })
        ]);
        console.info(lineResponse);
        console.info(directionResponse);
        console.info(stationResponse);
        const lineSuccess =  lineResponse.success;
        const directionSuccess =  directionResponse.success;
        const stationSuccess =  stationResponse.success;
        if (!lineSuccess || !directionSuccess || !stationSuccess) {
          if (!lineSuccess) {
            message.error(`${lineResponse.errorCode}:${lineResponse.errorMsg}`); // 打印错误信息
          }
          if (!directionSuccess) {
            message.error(`${directionResponse.errorCode}:${directionResponse.errorMsg}`); // 打印错误信息
          }
          if (!stationSuccess) {
            message.error(`${stationResponse.errorCode}:${stationResponse.errorMsg}`); // 打印错误信息
          }
        } else {
          // const systemData = systemResponse.result.rows;
          // const clusterData = clusterResponse.result.rows;
          const { lineData } = lineResponse.result;
          const { stationData } = stationResponse.result;
          const result  = {
            directionData: [...directionResponse.result],
            stationData,
            lineData
          };
          console.info(result);
          yield put({ type: 'updateState', payload: result });
        }
      } catch(e) {
        console.error(e);
        message.error('数据获取失败');
      }
    },
    *fetchAllLineOpe({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(fetchAllLineOpe, payload);
        const { success } = response;
        if (success) {
          const { result } = response;
          yield put({ type: 'updateState', payload: result });
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败');
      }
    },
    *addLineOpe({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(addLineOpe, payload);
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('新增失败');
        return false;
      }
    },
    *updateLineOpe({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(updateLineOpe, payload);
        const { success } = response;
        if (success) {
          message.success('更新成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('更新失败');
        return false;
      }
    },
    *deleteLineOpes({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(deleteLineOpes, payload);
        const { success } = response;
        if (success) {
          message.success('删除成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('删除失败');
        return false;
      }
    },
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default LineOpeModel;