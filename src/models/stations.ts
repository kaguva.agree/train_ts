import { Effect, Reducer } from 'umi';
import { message } from 'antd';
import { StationItem } from '@/pages/Urban/Station/data';
import { fetchAllStation, addStation, updateStation, deleteStations } from '@/pages/Urban/Station/service';
import { DataItem } from './common';
import { TradeResult } from './connect';

export interface StationModelState {
  rows?: StationItem[];
  total?: number;
  statusData?: DataItem[],
}

export interface StationModelType {
  namespace: 'stations';
  state: StationModelState;
  effects: {
    fetchAllStation: Effect;
    addStation: Effect;
    updateStation: Effect;
    deleteStations: Effect;
  };
  reducers: {
    updateState: Reducer<StationModelState>;
  };
}

const StationModel: StationModelType = {
  namespace: 'stations',

  state: {
    rows: [],
    total: 0,
    statusData: [
      {
        key: '0',
        value: '正常'
      },
      {
        key: '1',
        value: '异常'
      }
    ],
  },

  effects: {
    *fetchAllStation({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(fetchAllStation, payload);
        const { success } = response;
        if (success) {
          const { result } = response;
          yield put({ type: 'updateState', payload: result });
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败');
      }
    },
    *addStation({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(addStation, payload);
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('新增失败');
        return false;
      }
    },
    *updateStation({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(updateStation, payload);
        const { success } = response;
        if (success) {
          message.success('更新成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('更新失败');
        return false;
      }
    },
    *deleteStations({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(deleteStations, payload);
        const { success } = response;
        if (success) {
          message.success('删除成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('删除失败');
        return false;
      }
    },
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default StationModel;