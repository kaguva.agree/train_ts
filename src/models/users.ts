import { Effect, Reducer } from 'umi';
import { message } from 'antd';
import { UserItem } from '@/pages/Base/User/data';
import { fetchAllUser,
  addUser,
  updateUser,
  deleteUsers,
  fetchRolesByUser,
  assignRolesToUser,
  fetchUsersByRole } from '@/services/userApis';
import { DataItem } from './common';
import { TradeResult } from './connect';

export interface UserModelStateEx {
  rows?: UserItem[];
  total?: number;
  auditorData?: DataItem[],
  designerData?: DataItem[],
  developerData?: DataItem[],
  testerData?: DataItem[],
  statusData?: DataItem[],
}

export interface UserModelTypeEx {
  namespace: 'users';
  state: UserModelStateEx;
  effects: {
    fetchAllUser: Effect;
    addUser: Effect;
    updateUser: Effect;
    deleteUsers: Effect;
    /**
     * 查询指定角色下有哪些用户
     */
    fetchUsersByRole: Effect;
    fetchRolesByUser: Effect;
    assignRolesToUser: Effect;
    updateUserState: Effect;
  };
  reducers: {
    updateState: Reducer<UserModelStateEx>;
  };
}

const UserModelEx: UserModelTypeEx = {
  namespace: 'users',

  state: {
    rows: [],
    total: 0,
    statusData: [
      {
        key: '0',
        value: '生效'
      },
      {
        key: '1',
        value: '失效'
      }
    ],
  },

  effects: {
    *fetchAllUser({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(fetchAllUser, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          // 通讯成功取出数据
          const { result } = response;
          // 更新表格数据
          yield put({ type: 'updateState', payload: result });
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败'); // 打印错误信息
      }
    },
    *addUser({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(addUser, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('新增失败'); // 打印错误信息
        return false;
      }
    },
    *updateUser({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(updateUser, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('更新成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('更新失败'); // 打印错误信息
        return false;
      }
    },
    *deleteUsers({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(deleteUsers, payload);
        // console.log(JSON.stringify(response));
        // yield call(delay, 3000);
        const { success } = response;
        if (success) {
          message.success('删除成功');
          // 刷新表格
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('删除失败'); // 打印错误信息
        return false;
      }
    },
    *fetchUsersByRole({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(fetchUsersByRole, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          const { result } = response;
          yield put({ type: 'updateState', payload: result });
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('查询角色列表失败'); // 打印错误信息
        return false;
      }
    },
    *fetchRolesByUser({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(fetchRolesByUser, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          // 刷新表格
          return response;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('查询角色列表失败'); // 打印错误信息
        return false;
      }
    },
    *assignRolesToUser({ payload }, sagaEffects) {
      const { call } = sagaEffects;
      try {
        // 获取服务端数据
        // call 第一个参数为一个函数，后面的参数为函数调用时的入参
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(assignRolesToUser, payload);
        // console.log(JSON.stringify(response));
        const { success } = response;
        if (success) {
          message.success('分配角色成功');
          // 刷新表格
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('分配角色失败'); // 打印错误信息
        return false;
      }
    },
    *updateUserState({ payload }, sagaEffects) {
      console.info('updateUserState', payload);
      const { put } = sagaEffects;
      yield put({ type: 'updateState', payload });
    }
  },

  reducers: {
    updateState(state, { payload }) {
      console.info('users.updateState', payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default UserModelEx;