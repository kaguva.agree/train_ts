import { stringify } from 'querystring';
import { startsWith } from 'lodash';
import { history, Reducer, Effect } from 'umi';
import { fakeAccountLogin } from '@/services/login';
import { deleteCurrentLoginUserId,
  deleteToken,
  saveCurrentLoginUserId,
  saveToken,
  setAuthority,
  saveCurrentLoginUser } from '@/utils/authority';
import { getPageQuery } from '@/utils/utils';
import { TradeResult } from './connect';

export interface StateType {
  status?: 'ok' | 'error';
  type?: string;
  currentAuthority?: 'user' | 'guest' | 'admin';
}

export interface LoginModelType {
  namespace: string;
  state: StateType;
  effects: {
    login: Effect;
    logout: Effect;
  };
  reducers: {
    changeLoginStatus: Reducer<StateType>;
  };
}

const loginModel: LoginModelType = {
  namespace: 'login',

  state: {
    status: undefined,
  },

  effects: {
    *login({ payload }, { call, put }) {
      const response: TradeResult = yield call(fakeAccountLogin, payload);
      const { success } = response;
      if (success) {
        try {
          // message.success('登录成功');
          const { result } = response;
          const { status, token, userId, type, userName } = result;
          payload = {
            status,
            type,
          };
          saveToken(token);
          saveCurrentLoginUserId(userId);
          saveCurrentLoginUser(userName);
          yield put({
            type: 'changeLoginStatus',
            payload
          });
          const userPayload = {
            userid: userId
          };
          yield put({
            type: 'user/saveCurrentUser',
            payload: userPayload
          });
          // 是否存在跳转url
          console.info(window.location);
          const urlParams = new URL(window.location.href);
          const params = getPageQuery();
          let { redirect } = params as { redirect: string };
          if (redirect) {
            // 跳转参数不是http开头，则需要添加
            const { origin } = window.location;
            if (!startsWith(redirect, origin)) {
              redirect = origin + redirect;
            }
            console.info(redirect);
            const redirectUrlParams = new URL(redirect);
            if (redirectUrlParams.origin === urlParams.origin) {
              redirect = redirect.substr(urlParams.origin.length);
              if (redirect.match(/^\/.*#/)) {
                redirect = redirect.substr(redirect.indexOf('#') + 1);
              }
            } else {
              window.location.href = '/';
              return;
            }
          }
          history.replace(redirect || '/welcome');
        } catch (error) {
          deleteToken();
          deleteCurrentLoginUserId();
          console.error(error);
        }
      } else {
        const { errorMsg } = response;
        payload = {
          status: 'error',
          type: 'account',
          errorMsg
        }
        yield put({
          type: 'changeLoginStatus',
          payload
        });
      }
    },
    *logout() {
      const { redirect } = getPageQuery();
      // Note: There may be security issues, please note
      if (window.location.pathname !== '/user/login' && !redirect) {
        deleteToken();
        deleteCurrentLoginUserId();
        history.replace({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href,
          }),
        });
      }
    },
  },

  reducers: {
    changeLoginStatus(state, { payload }) {
      setAuthority(payload.currentAuthority);
      return {
        ...state,
        status: payload.status,
        type: payload.type,
        errorMsg: payload.errorMsg,
      };
    },
  },
};

export default loginModel;