import { MenuDataItem, Settings as ProSettings } from '@ant-design/pro-layout';
import { GlobalModelState } from './global';
import { UserModelState } from './user';
import { StateType } from './login';
import { MenuModelState }from './menu';
import { ParamModelState } from './param';
import { DictModelState } from './dicts';
import { MenuModelStateEx } from './menus';
import { RoleModelState } from './roles';
import { SystemModelState } from './systems';
import { UserModelStateEx } from './users';
import { TrainModelState } from './trains';
import { StationModelState } from './stations';
import { LineModelState } from './lines';
import { LineOpeModelState } from './lineOpes';

export { GlobalModelState, UserModelState };

export interface Loading {
  global: boolean;
  effects: { [key: string]: boolean | undefined };
  models: {
    global?: boolean;
    menu?: boolean;
    setting?: boolean;
    user?: boolean;
    login?: boolean;
  };
}

export interface ConnectState {
  global: GlobalModelState;
  loading: Loading;
  settings: ProSettings;
  user: UserModelState;
  login: StateType;
  menu: MenuModelState;
  params: ParamModelState;
  dicts: DictModelState;
  menus: MenuModelStateEx;
  roles: RoleModelState;
  systems: SystemModelState;
  users: UserModelStateEx;
  trains: TrainModelState;
  stations: StationModelState;
  lines: LineModelState;
  lineopes: LineOpeModelState;
}

export interface Route extends MenuDataItem {
  routes?: Route[];
}

export interface TradeResult {
  success: boolean;
  errorCode: string;
  errorMsg: string;
  result: any;
}