export interface BaseModelType<T> {
  reducers: {
    updateState: Reducer<T>;
  };
}

/**
 * 基础Model，方便其他业务Model继承该对象，减少额外的代码
 */
const baseModel: BaseModelType = {
  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    }
  }
}

export {
  baseModel
}

/**
 * 操作类型枚举，view-查看，edit-更新
 */
export type FlagEnum = 'view' | 'edit' | 'add' | 'delete';

/**
 * 选项模型类
 */
export interface DataItem {
  /** 选项key值 */
  key: string;
  /** 选项value值 */
  value: string;
}