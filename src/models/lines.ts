import { Effect, Reducer } from 'umi';
import { message } from 'antd';
import { LineItem } from '@/pages/Urban/Line/data';
import { fetchAllLine, addLine, updateLine, deleteLines } from '@/pages/Urban/Line/service';
import { fetchAllStationForSelect } from '@/pages/Urban/Station/service';
import { querySysDict } from '@/services/systemApis';
import { DataItem } from './common';
import { TradeResult } from './connect';

export interface LineModelState {
  rows?: LineItem[];
  total?: number;
  directionData: DataItem[],
  stationData?: DataItem[],
  statusData?: DataItem[],
}

export interface LineModelType {
  namespace: 'lines';
  state: LineModelState;
  effects: {
    fetchLineInitParams: Effect;
    fetchAllLine: Effect;
    addLine: Effect;
    updateLine: Effect;
    deleteLines: Effect;
  };
  reducers: {
    updateState: Reducer<LineModelState>;
  };
}

const LineModel: LineModelType = {
  namespace: 'lines',

  state: {
    rows: [],
    total: 0,
    directionData: [],
    stationData: [],
    statusData: [
      {
        key: '0',
        value: '正常'
      },
      {
        key: '1',
        value: '异常'
      }
    ],
  },

  effects: {
    *fetchLineInitParams({ payload }, sagaEffects) {
      const { all, call, put } = sagaEffects;
      try {
        const [ stationResponse, directionResponse ] = yield all([
          call(fetchAllStationForSelect, {}),
          call(querySysDict, {
            dictName: 'direction'
          })
        ]);
        console.info(directionResponse);
        console.info(stationResponse);
        const directionSuccess =  directionResponse.success;
        const stationSuccess =  stationResponse.success;
        if (!directionSuccess || !stationSuccess) {
          if (!directionSuccess) {
            message.error(`${directionResponse.errorCode}:${directionResponse.errorMsg}`); // 打印错误信息
          }
          if (!stationSuccess) {
            message.error(`${stationResponse.errorCode}:${stationResponse.errorMsg}`); // 打印错误信息
          }
        } else {
          // const systemData = systemResponse.result.rows;
          // const clusterData = clusterResponse.result.rows;
          const { stationData } = stationResponse.result;
          const result  = {
            directionData: [...directionResponse.result],
            stationData
          };
          console.info(result);
          yield put({ type: 'updateState', payload: result });
        }
      } catch(e) {
        console.error(e);
        message.error('数据获取失败');
      }
    },
    *fetchAllLine({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(fetchAllLine, payload);
        const { success } = response;
        if (success) {
          const { result } = response;
          yield put({ type: 'updateState', payload: result });
        }
      } catch(e) {
        console.info(e);
        message.error('数据获取失败');
      }
    },
    *addLine({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(addLine, payload);
        const { success } = response;
        if (success) {
          message.success('新增成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('新增失败');
        return false;
      }
    },
    *updateLine({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(updateLine, payload);
        const { success } = response;
        if (success) {
          message.success('更新成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.info(e);
        message.error('更新失败');
        return false;
      }
    },
    *deleteLines({ payload }, sagaEffects) {
      const { call, put } = sagaEffects;
      try {
        console.log(JSON.stringify(payload));
        const response: TradeResult = yield call(deleteLines, payload);
        const { success } = response;
        if (success) {
          message.success('删除成功');
          return true;
        } else {
          return false;
        }
      } catch(e) {
        console.error(e);
        message.error('删除失败');
        return false;
      }
    },
  },

  reducers: {
    updateState(state, { payload }) {
      console.info(payload);
      return {
        ...state,
        ...payload
      }
    },
  },
}

export default LineModel;