export const DICT_NAMESPACE = 'dicts';

/**
 * 系统字典清单查询
 * @Dict {*} payload 查询条件
 * @returns 系统字典清单
 */
export function DICT_LIST(payload: {}) {
  return {
    type: `${DICT_NAMESPACE}/fetchAllDict`,
    payload,
  };
}

/**
 * 新增系统字典
 * @Dict {*} payload 系统字典信息
 * @returns true-新增成功
 */
export function DICT_ADD(payload: {}) {
  return {
    type: `${DICT_NAMESPACE}/addDict`,
    payload,
  };
}

/**
 * 更新系统字典信息
 * @Dict {*} payload 系统字典信息
 * @returns true-更新成功
 */
export function DICT_UPDATE(payload: {}) {
  return {
    type: `${DICT_NAMESPACE}/updateDict`,
    payload,
  };
}

/**
 * 删除系统字典
 * @Dict {*} payload 系统字典id集合
 * @returns true-删除成功
 */
export function DICT_DELETE(payload: {}) {
  return {
    type: `${DICT_NAMESPACE}/deleteDicts`,
    payload,
  };
}

/**
 * 更新props
 * @Dict {*} payload 待更新的数据
 * @returns true-
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${DICT_NAMESPACE}/updateDictState`,
    payload,
  };
}