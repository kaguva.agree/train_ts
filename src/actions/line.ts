export const LINE_NAMESPACE = 'lines';

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 参数
 */
export function LINE_INIT(payload: {}) {
  return {
    type: `${LINE_NAMESPACE}/fetchLineInitParams`,
    payload,
  };
}

/**
 * 线路信息清单查询
 * @Line {*} payload 查询条件
 * @returns 线路信息清单
 */
export function LINE_LIST(payload: {}) {
  return {
    type: `${LINE_NAMESPACE}/fetchAllLine`,
    payload,
  };
}

/**
 * 新增线路信息
 * @Line {*} payload 线路信息信息
 * @returns true-新增成功
 */
export function LINE_ADD(payload: {}) {
  return {
    type: `${LINE_NAMESPACE}/addLine`,
    payload,
  };
}

/**
 * 更新线路信息信息
 * @Line {*} payload 线路信息信息
 * @returns true-更新成功
 */
export function LINE_UPDATE(payload: {}) {
  return {
    type: `${LINE_NAMESPACE}/updateLine`,
    payload,
  };
}

/**
 * 删除线路信息
 * @Line {*} payload 线路信息id集合
 * @returns true-删除成功
 */
export function LINE_DELETE(payload: {}) {
  return {
    type: `${LINE_NAMESPACE}/deleteLines`,
    payload,
  };
}

/**
 * 更新props
 * @param {*} payload 待更新的数据
 * @returns true-更新成功
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${LINE_NAMESPACE}/updateState`,
    payload,
  };
}