export const MENU_NAMESPACE = 'menus';

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 参数
 */
export function MENU_INIT(payload: {}) {
  return {
    type: `${MENU_NAMESPACE}/fetchMenuInitParams`,
    payload,
  };
}

/**
 * 菜单清单查询
 * @param {*} payload 查询条件
 * @returns 菜单清单
 */
export function MENU_LIST(payload: {}) {
  return {
    type: `${MENU_NAMESPACE}/fetchAllMenu`,
    payload,
  };
}

/**
 * 新增菜单
 * @param {*} payload 菜单信息
 * @returns true-新增成功
 */
export function MENU_ADD(payload: {}) {
  return {
    type: `${MENU_NAMESPACE}/addMenu`,
    payload,
  };
}

/**
 * 更新菜单信息
 * @param {*} payload 菜单信息
 * @returns true-更新成功
 */
export function MENU_UPDATE(payload: {}) {
  return {
    type: `${MENU_NAMESPACE}/updateMenu`,
    payload,
  };
}

/**
 * 删除菜单
 * @param {*} payload 菜单id集合
 * @returns true-删除成功
 */
export function MENU_DELETE(payload: {}) {
  return {
    type: `${MENU_NAMESPACE}/deleteMenus`,
    payload,
  };
}

/**
 * 更新props
 * @param {*} payload 待更新的数据
 * @returns true-更新成功
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${MENU_NAMESPACE}/updateMenuState`,
    payload,
  };
}