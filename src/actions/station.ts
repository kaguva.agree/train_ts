export const STATION_NAMESPACE = 'stations';

/**
 * 站点信息清单查询
 * @Station {*} payload 查询条件
 * @returns 站点信息清单
 */
export function STATION_LIST(payload: {}) {
  return {
    type: `${STATION_NAMESPACE}/fetchAllStation`,
    payload,
  };
}

/**
 * 新增站点信息
 * @Station {*} payload 站点信息信息
 * @returns true-新增成功
 */
export function STATION_ADD(payload: {}) {
  return {
    type: `${STATION_NAMESPACE}/addStation`,
    payload,
  };
}

/**
 * 更新站点信息信息
 * @Station {*} payload 站点信息信息
 * @returns true-更新成功
 */
export function STATION_UPDATE(payload: {}) {
  return {
    type: `${STATION_NAMESPACE}/updateStation`,
    payload,
  };
}

/**
 * 删除站点信息
 * @Station {*} payload 站点信息id集合
 * @returns true-删除成功
 */
export function STATION_DELETE(payload: {}) {
  return {
    type: `${STATION_NAMESPACE}/deleteStations`,
    payload,
  };
}

/**
 * 更新props
 * @param {*} payload 待更新的数据
 * @returns true-更新成功
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${STATION_NAMESPACE}/updateState`,
    payload,
  };
}