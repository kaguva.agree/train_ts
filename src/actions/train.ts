export const TRAIN_NAMESPACE = 'trains';

/**
 * 列车信息清单查询
 * @Train {*} payload 查询条件
 * @returns 列车信息清单
 */
export function TRAIN_LIST(payload: {}) {
  return {
    type: `${TRAIN_NAMESPACE}/fetchAllTrain`,
    payload,
  };
}

/**
 * 新增列车信息
 * @Train {*} payload 列车信息信息
 * @returns true-新增成功
 */
export function TRAIN_ADD(payload: {}) {
  return {
    type: `${TRAIN_NAMESPACE}/addTrain`,
    payload,
  };
}

/**
 * 更新列车信息信息
 * @Train {*} payload 列车信息信息
 * @returns true-更新成功
 */
export function TRAIN_UPDATE(payload: {}) {
  return {
    type: `${TRAIN_NAMESPACE}/updateTrain`,
    payload,
  };
}

/**
 * 删除列车信息
 * @Train {*} payload 列车信息id集合
 * @returns true-删除成功
 */
export function TRAIN_DELETE(payload: {}) {
  return {
    type: `${TRAIN_NAMESPACE}/deleteTrains`,
    payload,
  };
}

/**
 * 更新props
 * @param {*} payload 待更新的数据
 * @returns true-更新成功
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${TRAIN_NAMESPACE}/updateState`,
    payload,
  };
}