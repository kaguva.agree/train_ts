export const LINEOPE_NAMESPACE = 'lineopes';

/**
 * 查询本页面需所有参数
 * @App {*} payload 查询条件
 * @returns 参数
 */
export function LINEOPE_INIT(payload: {}) {
  return {
    type: `${LINEOPE_NAMESPACE}/fetchLineOpeInitParams`,
    payload,
  };
}

/**
 * 线路信息清单查询
 * @LineOpe {*} payload 查询条件
 * @returns 线路信息清单
 */
export function LINEOPE_LIST(payload: {}) {
  return {
    type: `${LINEOPE_NAMESPACE}/fetchAllLineOpe`,
    payload,
  };
}

/**
 * 新增线路信息
 * @LineOpe {*} payload 线路信息信息
 * @returns true-新增成功
 */
export function LINEOPE_ADD(payload: {}) {
  return {
    type: `${LINEOPE_NAMESPACE}/addLineOpe`,
    payload,
  };
}

/**
 * 更新线路信息信息
 * @LineOpe {*} payload 线路信息信息
 * @returns true-更新成功
 */
export function LINEOPE_UPDATE(payload: {}) {
  return {
    type: `${LINEOPE_NAMESPACE}/updateLineOpe`,
    payload,
  };
}

/**
 * 删除线路信息
 * @LineOpe {*} payload 线路信息id集合
 * @returns true-删除成功
 */
export function LINEOPE_DELETE(payload: {}) {
  return {
    type: `${LINEOPE_NAMESPACE}/deleteLineOpes`,
    payload,
  };
}

/**
 * 更新props
 * @param {*} payload 待更新的数据
 * @returns true-更新成功
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${LINEOPE_NAMESPACE}/updateState`,
    payload,
  };
}