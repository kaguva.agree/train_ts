export const USER_NAMESPACE = 'users';

/**
 * 用户清单查询
 * @param {*} payload 查询条件
 * @returns 用户清单
 */
export function USER_LIST(payload: {}) {
  return {
    type: `${USER_NAMESPACE}/fetchAllUser`,
    payload,
  };
}

/**
 * 新增用户
 * @param {*} payload 用户信息
 * @returns true-新增成功
 */
export function USER_ADD(payload: {}) {
  return {
    type: `${USER_NAMESPACE}/addUser`,
    payload,
  };
}

/**
 * 更新用户信息
 * @param {*} payload 用户信息
 * @returns true-更新成功
 */
export function USER_UPDATE(payload: {}) {
  return {
    type: `${USER_NAMESPACE}/updateUser`,
    payload,
  };
}

/**
 * 删除用户
 * @param {*} payload 用户id集合
 * @returns true-删除成功
 */
export function USER_DELETE(payload: {}) {
  return {
    type: `${USER_NAMESPACE}/deleteUsers`,
    payload,
  };
}

/**
 * 重置用户密码
 * @param {*} payload 用户id集合
 * @returns true-重置成功
 */
export function USER_RESET_PASS(payload: {}) {
  return {
    type: `${USER_NAMESPACE}/resetUserPass`,
    payload,
  };
}

/**
 * 用户分配角色
 * @param {*} payload 用户id集合
 * @returns true-分配成功
 */
export function USER_ASSIGN_ROLE(payload: {}) {
  return {
    type: `${USER_NAMESPACE}/assignRolesToUser`,
    payload,
  };
}

/**
 * 查询用户已分配角色列表
 * @param {*} payload 用户id集合
 * @returns true-分配成功
 */
export function USER_ROLE(payload: {}) {
  return {
    type: `${USER_NAMESPACE}/fetchRolesByUser`,
    payload,
  };
}

/**
 * 更新props
 * @param {*} payload 待更新的数据
 * @returns true-更新成功
 */
export function UPDATE_STATE(payload: {}) {
  return {
    type: `${USER_NAMESPACE}/updateUserState`,
    payload,
  };
}