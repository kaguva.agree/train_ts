import { Request, Response } from 'express';
const fs = require('fs');
const data = fs.readFileSync('D:/giteeworkspace/train_ts/mock/menuList.json');
const menuData = JSON.parse(data);

const data1 = fs.readFileSync('D:/giteeworkspace/train_ts/mock/userMenu.json');
const userMenu = JSON.parse(data1);

async function getFakeMenuData(req: Request, res: Response) {
  return res.json(menuData);
}

async function getFakeUserMenuData(req: Request, res: Response) {
  return res.json(userMenu);
}

// 代码中会兼容本地 service mock 以及部署站点的静态数据
export default {
  // 支持值为 Object 和 Array
  'GET /api/user/menu/31': getFakeUserMenuData,
  'POST /dev/menu/list': getFakeMenuData
};