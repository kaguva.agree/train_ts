import { Request, Response } from 'express';
const fs = require('fs');
const data = fs.readFileSync('D:/giteeworkspace/train_ts/mock/dictList.json');
const dictData = JSON.parse(data);

async function getFakDictData(req: Request, res: Response) {
  return res.json(dictData);
}

// 代码中会兼容本地 service mock 以及部署站点的静态数据
export default {
  // 支持值为 Object 和 Array
  'POST /dev/sysdict/list': getFakDictData
};