import { RoleItem } from '@/pages/Base/Role/data';
import { Request, Response } from 'express';
import Mock from 'mockjs';

var Random = Mock.Random;

const genList = (current: number, pageSize: number) => {
  const roleListDataSource: RoleItem[] = [];

  for (let i = 0; i < pageSize; i += 1) {
    const index = (current - 1) * pageSize + i;
    roleListDataSource.push({
      roleId: index + '',
      roleCode: Random.name(),
      roleName: Random.cname(),
      roleDesc: Random.cname(),
      roleStatus: Random.integer(0, 1) + '',
    });
  }
  // trainListDataSource.reverse();
  return roleListDataSource;
};

// 默认生成100条数据
let roleListDataSource = genList(1, 100);
let total = roleListDataSource.length;

async function fetchAllRole(req: Request, res: Response) {
  const { body } = req;
  const { pageNum, pageSize } = body;
  let dataSource = [...roleListDataSource].slice(
    (pageNum - 1) * pageSize , pageNum * pageSize,
  );
  const result = {
    success: true,
    result: {
      rows: dataSource,
      total,
    }
  };
  return res.json(result);
}

// 代码中会兼容本地 service mock 以及部署站点的静态数据
export default {
  'POST /dev/role/list': fetchAllRole,
};
