// eslint-disable-next-line import/no-extraneous-dependencies
import { Request, Response } from 'express';
import { parse } from 'url';
import { StationListItem, StationListParams } from '@/pages/Station/data';
import Mock from 'mockjs';

const stationNames = [
  '保顺站',
  '华山站',
  '泰山站',
  '衡山站',
  '龙山站',
  '鞍山站',
  '港湾站',
  '裕安站',
  '武夷山站',
  '港一站',
  '天柱山站',
  '天门山站',
  '中山北站',
  '环城北路站',
  '奥体中心站',
  '芜湖南站',
  '白马山站',
]

var Random = Mock.Random;
// mock tableListDataSource
const genList = (current: number, pageSize: number) => {
  const trainListDataSource: StationListItem[] = [];

  for (let i = 0; i < stationNames.length; i += 1) {
    trainListDataSource.push({
      id: i + '',
      name: stationNames[i],
      latitude: Math.floor(Math.random() * 1000),
      longitude: Math.floor(Math.random() * 1000),
      address: Random.county(true),
      status: '0'
    });
  }
  trainListDataSource.reverse();
  return trainListDataSource;
};

let trainListDataSource = genList(1, 100);

function getStations(req: Request, res: Response, u: string) {
  let realUrl = u;
  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }
  const { current = 1, pageSize = 10 } = req.query;
  const params = (parse(realUrl, true).query as unknown) as StationListParams;

  let dataSource = [...trainListDataSource].slice(
    ((current as number) - 1) * (pageSize as number),
    (current as number) * (pageSize as number),
  );
  const sorter = JSON.parse(params.sorter as any);
  if (sorter) {
    dataSource = dataSource.sort((prev, next) => {
      let sortNumber = 0;
      Object.keys(sorter).forEach((key) => {
        if (sorter[key] === 'descend') {
          if (prev[key] - next[key] > 0) {
            sortNumber += -1;
          } else {
            sortNumber += 1;
          }
          return;
        }
        if (prev[key] - next[key] > 0) {
          sortNumber += 1;
        } else {
          sortNumber += -1;
        }
      });
      return sortNumber;
    });
  }
  if (params.filter) {
    const filter = JSON.parse(params.filter as any) as {
      [key: string]: string[];
    };
    if (Object.keys(filter).length > 0) {
      dataSource = dataSource.filter((item) => {
        return Object.keys(filter).some((key) => {
          if (!filter[key]) {
            return true;
          }
          if (filter[key].includes(`${item[key]}`)) {
            return true;
          }
          return false;
        });
      });
    }
  }
  const result = {
    data: dataSource,
    total: trainListDataSource.length,
    success: true,
    pageSize,
    current: parseInt(`${params.currentPage}`, 10) || 1,
  };

  return res.json(result);
}

function postStation(req: Request, res: Response, u: string, b: Request) {
  let realUrl = u;
  if (!realUrl || Object.prototype.toString.call(realUrl) !== '[object String]') {
    realUrl = req.url;
  }

  const body = (b && b.body) || req.body;
  const { method, name, desc, id } = body;

  switch (method) {
    /* eslint no-case-declarations:0 */
    case 'delete':
      trainListDataSource = trainListDataSource.filter((item) => id.indexOf(item.id) === -1);
      break;
    case 'post':
      (() => {
        const newTrain = {
          id: trainListDataSource.length + '',
          num: `B1${Math.floor(Math.random() * 10) % 2}`,
          capacity: Math.floor(Math.random() * 1000),
          line: '二号线',
          status: '1'
        };
        trainListDataSource.unshift(newTrain);
        return res.json(newTrain);
      })();
      return;

    case 'update':
      (() => {
        let newTrain = {};
        trainListDataSource = trainListDataSource.map((item) => {
          if (item.id === id) {
            newTrain = { ...item, desc, name };
            return { ...item, desc, name };
          }
          return item;
        });
        return res.json(newTrain);
      })();
      return;
    default:
      break;
  }

  const result = {
    list: trainListDataSource,
    pagination: {
      total: trainListDataSource.length,
    },
  };

  res.json(result);
}

export default {
  'GET /api/station': getStations,
  'POST /api/station': postStation,
};
