import { Request, Response } from 'express';
const fs = require('fs');
const data = fs.readFileSync('D:/giteeworkspace/train_ts/mock/paramList.json');
const paramData = JSON.parse(data);

async function getFakParamData(req: Request, res: Response) {
  return res.json(paramData);
}

// 代码中会兼容本地 service mock 以及部署站点的静态数据
export default {
  // 支持值为 Object 和 Array
  'POST /dev/param/list': getFakParamData
};