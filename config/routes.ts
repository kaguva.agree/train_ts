﻿export default [
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      {
        path: '/user/login',
        name: 'login',
        component: './User/Login',
      },
    ],
  },
  {
    path: '/',
    component: '../layouts/SecurityLayout',
    routes: [
      {
        path: '/',
        icon: 'crown',
        component: '../layouts/BasicLayout',
        authority: ['admin', 'user'],
        routes: [
          {
            path: '/welcome',
            name: 'welcome',
            icon: 'smile',
            component: './Welcome',
          },
          {
            path: '/base',
            name: 'base',
            icon: 'safety',
            routes: [
              {
                path: '/base/user/list',
                name: 'user-list',
                icon: 'smile',
                component: './Base/User',
              },
              {
                path: '/base/role/list',
                name: 'role-list',
                icon: 'smile',
                component: './Base/Role',
              },
              {
                path: '/base/menu/list',
                name: 'menu-list',
                icon: 'smile',
                component: './Base/Menu',
              },
            ]
          },
          {
            path: '/system',
            name: 'system',
            icon: 'setting',
            routes: [
              {
                path: '/system/param/list',
                name: 'param-list',
                icon: 'smile',
                component: './System/Param',
              },
              {
                path: '/system/dict/list',
                name: 'dict-list',
                icon: 'smile',
                component: './System/Dict',
              },
            ]
          },
          {
            path: '/urban',
            name: 'urban',
            icon: 'table',
            routes: [
              {
                path: '/urban/train/list',
                name: 'train-list',
                icon: 'smile',
                component: './Urban/Train',
              },
              {
                path: '/urban/station/list',
                name: 'station-list',
                icon: 'smile',
                component: './Urban/Station',
              },
              {
                path: '/urban/line/list',
                name: 'line-list',
                icon: 'smile',
                component: './Urban/Line',
              },
              {
                path: '/urban/lineope/list',
                name: 'line-ope-list',
                icon: 'smile',
                component: './Urban/LineOpe',
              },
              {
                component: './404',
              },
            ]
          }
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    component: './404',
  },
];
